$(document).ready(function () {

    var defaultValue = [];
    defaultValue['book[name]'] = 'Name';
    defaultValue['book[email]'] = 'Email';

    $(document).on('blur', '.list_last input',function () {

        if ($(this).val() == '') {
            $(this).val(defaultValue[$(this).attr('name')]);
        }

    }).on('click', '.list_last input', function () {

        if ($(this).val() == defaultValue[$(this).attr('name')]) {
            $(this).val('');
        }

    });

    $(document).on('click', '.send_book', function () {

        var url = $(this).data('action');
        var fields = $(this).parents('.list_last').find('input');
        var product = $('.hidden.book').data('product');
        var data = {'book[product]': product};

        $(fields).each(function (key, value) {

            if ($(value).data('default')) {
                if ($(value).val() != $(value).data('default')) {
                    data[$(value).attr('name')] = $(value).val();
                }
            } else {
                data[$(value).attr('name')] = $(value).val();
            }

        });

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: data

        }).done(function (data) {

            if (data.success == true) {

                var viewContainer = $('.shadow_box .shadow_in');

                viewContainer.html(data.result);

                var closeCross = $('a.close_span', viewContainer);

                closeCross.click(function () {

                    hideShadowBox();
                    return false;

                });
            }

        });

    });

});

