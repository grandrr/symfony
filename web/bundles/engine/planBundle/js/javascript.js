$(document).ready(function () {

    var form = $('.formContainer');

    var button = $('.black_basic > a');

    button.click(function () {

        plan = $(this).data('plan');
        showShadowBox(form.html());

        processForm();

        return false;

    });

});

function processForm() {
    var sendButton = $('a.for_sign');
    var url = sendButton.data('action');
    var fields = $('.shadow_in .list_last input');

    var defaultValue = [];
    defaultValue['plan[username]'] = 'Username';
    defaultValue['plan[email]'] = 'Email';
    defaultValue['plan[password]'] = 'Password';

    fields.blur(function () {

        if ($(this).val() == '') {
            $(this).val(defaultValue[$(this).attr('name')]);
        }

    }).click(function () {

        if ($(this).val() == defaultValue[$(this).attr('name')]) {
            $(this).val('');
        }

    });


    sendButton.click(function () {

        var data = {'plan[plan]': plan};

        $(fields).each(function (key, value) {

            data[$(value).attr('name')] = $(value).val();

        });

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: data

        }).done(function (data) {
            if (data.success == true) {

                var viewContainer = $('.shadow_box .shadow_in');

                viewContainer.html(data.result);

                var closeCross = $('a.close_span', viewContainer);

                closeCross.click(function () {

                    hideShadowBox();
                    return false;

                });
            } else {
                $('.shadow_in .list_last').html(data.result);
                processForm();
            }
        });

    });
}