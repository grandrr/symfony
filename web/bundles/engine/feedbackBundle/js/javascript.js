$(document).ready(function () {

    var defaultValue = [];
    defaultValue['feedback[name]'] = 'Name';
    defaultValue['feedback[phone]'] = 'Phone';
    defaultValue['feedback[message]'] = 'Message';

    $(document).on('blur', '.list_last input, .list_last textarea', function(){

        if ($(this).val() == '') {
            $(this).val(defaultValue[$(this).attr('name')]);
        }

    }).on('click', '.list_last input, .list_last textarea', function(){

        if ($(this).val() == defaultValue[$(this).attr('name')]) {
            $(this).val('');
        }

    });

    $(document).on('click', '.send_message', function(){

        var url = $(this).data('action');
        var fields = $(this).parents('.list_last').find('textarea, input');
        var data = {};

        $(fields).each(function (key, value) {

            if($(value).data('default')){
                if($(value).val() != $(value).data('default')){
                    data[$(value).attr('name')] = $(value).val();
                }
            }else{
                data[$(value).attr('name')] = $(value).val();
            }

        });

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: data

        }).done(function (data) {
            if (data.success == true) {

                var viewContainer = $('.shadow_box .shadow_in');

                viewContainer.html(data.html);

                var closeCross = $('a.close_span', viewContainer);

                closeCross.click(function () {

                    hideShadowBox();
                    return false;

                });
            }
        });

    });

});

