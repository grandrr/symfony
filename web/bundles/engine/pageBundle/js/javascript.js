function pageTree() {

    $(document).ready(function () {

        var linkGetData = $('a.getAjaxData').attr('href');
        var linkUpdateData = $('a.updateAjaxData').attr('href');

        var setting = {
            async: {
                enable: true,
                url: linkGetData
            },
            view: {
                addDiyDom: addDiyDom,
                selectedMulti: false
            },
            edit: {
                enable: true,
                showRemoveBtn: false,
                showRenameBtn: false
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onDrop: onDrop,
                onRemove: onRemove,
                onAsyncSuccess: onAsync
            }
        };

        function onDrop(event, treeId, treeNodes, targetNode, moveType) {

            var currentNode = treeNodes[0];
            var changedNodes = null;
            var parentNode = currentNode.getParentNode();

            if (parentNode) {
                changedNodes = parentNode.children;
            } else {
                changedNodes = $.fn.zTree.getZTreeObj("tree").getNodes();
            }

            console.log(changedNodes);

            var sendData = [];

            $(changedNodes).each(function (key, value) {
                var page = {};
                page.id = value.id;
                page.pid = value.pId;
                sendData.push(page);
            });

            $.ajax({
                type: "POST",
                url: linkUpdateData,
                dataType: 'json',
                data: {data: sendData},
                success: function (data) {
                    console.log(data);
                }
            });
        }

        function onRemove(e, treeId, treeNode) {
            showLog("[ " + getTime() + " onRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
        }

        function addDiyDom(treeId, treeNode) {

            if (treeNode.editNameFlag || $("#publishBtn_" + treeNode.tId).length > 0) return;
            var sObj = $("#" + treeNode.tId + "_span");
            var clasPublish = treeNode.publish == true ? 'button chk checkbox_true_full' : 'button chk checkbox_false_full';
            var publishStr = "<span class='" + clasPublish + "' id='publishBtn_" + treeNode.tId
                + "' title='Publish/Unpublish' onfocus='this.blur();' data-id=" + treeNode.id + "></span>";

            var editStr = "<span class='button edit' id='editBtn_" + treeNode.tId
                + "' title='Edit' onfocus='this.blur();' data-id=" + treeNode.id + "></span>";

            var deleteStr = "<span class='button remove' id='removeBtn_" + treeNode.tId
                + "' title='Remove' onfocus='this.blur();' data-id=" + treeNode.id + "></span>";

            sObj.after(deleteStr).after(editStr).after(publishStr);

            $("#publishBtn_" + treeNode.tId).click(function () {

                var button = $(this);
                var link = $('.changePublishAjax').attr('href');
                var id = $(this).data('id');

                $.ajax({
                    type: "POST",
                    url: link,
                    dataType: 'json',
                    data: {id: id},
                    success: function (data) {
                        if (data.result == true) {
                            if (button.hasClass('checkbox_true_full')) {
                                button.removeClass('checkbox_true_full').addClass('checkbox_false_full');
                            } else {
                                button.removeClass('checkbox_false_full').addClass('checkbox_true_full');
                            }

//                        document.location.reload();
                        }
                    }
                });

            });

            $("#editBtn_" + treeNode.tId).click(function () {

                document.location.href = treeNode.editLink;

            });

            $("#removeBtn_" + treeNode.tId).click(function () {

                document.location.href = treeNode.removeLink;

            });
        }

        //add to unpublished item style
        function onAsync(event, treeId, treeNode, msg) {
            $(jQuery.parseJSON(msg)).each(function (key, value) {

                var node = tree.getNodeByParam("id", value.id, null);
                var item = $('#' + node.tId + '_span');

                if (value.publish == false) {
                    item.addClass('hiddenItem');
                }

            });

        };

        var tree = $.fn.zTree.init($("#tree"), setting);

    });

}

function pageEdit() {

    $(document).ready(function () {

        _tiny();

    });

}

function _tiny() {
    tinymce.init({
        selector: "textarea.tiny",
        theme: "modern",
        width: 'auto',
        height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        content_css: "css/content.css",
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | code",
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });
}