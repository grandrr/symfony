

    function showShadowBox(content) {

        var shadowBox = $('.shadow_box');
        var viewContainer = $('.shadow_in', shadowBox);

        viewContainer.html(content);
        shadowBox.show();

        shadowBox.click(function (el) {

            if ($(el.target).attr('class') == 'shadow_box')
                hideShadowBox();

        });

        var closeCross = $('a.close_span', viewContainer);

        closeCross.click(function () {

            hideShadowBox();
            return false;

        });

    }

    function hideShadowBox() {

        var shadowBox = $('.shadow_box');
        shadowBox.hide();
    }

    $(document).ready(function(){

        var link = $('.link');

        $(document).on('click', '.link', function(){

            var page = $(this).data('link');
            var content = $('.hidden.'+page).html();

            showShadowBox(content);

            return false;

        });

    });

