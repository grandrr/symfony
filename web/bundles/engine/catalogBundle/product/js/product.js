$(document).ready(function () {

    //$('#slider').nivoSlider(
    //    {manualAdvance: true, effect: 'sliceDown'}
    //);

    //jQuery('.d-carousel').jcarousel({
    //    scroll: 1
    //});

    //$('.small-carousel').jcarousel({
    //    scroll: 1,
    //    initCallback: function(carousel, nameFunction){
    //
    //        $('.small-carousel > li').click(function(){
    //
    //            var index = jQuery(this).index()+1;
    //            $('.big-carousel').jcarousel('scroll', index);
    //
    //        });
    //
    //    }
    //});
    //
    //$('.big-carousel').jcarousel({
    //    scroll: 1,
    //    buttonPrevHTML: '<a class="nivo-prevNav">Prev</a>',
    //    buttonNextHTML: '<a class="nivo-nextNav">Next</a>',
    //    itemVisibleInCallback: function(carousel, item, index, direction){
    //        $('.small-carousel').jcarousel('scroll', index);
    //        var smallItem = $('.small-carousel > li[jcarouselindex="'+index+'"]');
    //        smallItem.addClass('active');
    //    },
    //    itemVisibleOutCallback: function(carousel, item, index, direction){
    //        var smallItem = $('.small-carousel > li[jcarouselindex="'+index+'"]');
    //        smallItem.removeClass('active');
    //    }
    //});

    //$('.small-carousel').jcarousel({
    //    scroll: 1,
    //    initCallback: function(carousel, nameFunction){
    //
    //        $('.small-carousel > li').click(function(){
    //
    //            var index = jQuery(this).index()+1;
    //            $('.big-carousel').jcarousel('scroll', index);
    //
    //        });
    //
    //    }
    //});

    var carouselStage = $('.big-carousel').jcarousel({
        //scroll: 1,
        //buttonPrevHTML: '<a class="nivo-prevNav">Prev</a>',
        //buttonNextHTML: '<a class="nivo-nextNav">Next</a>'
    });

    $('.slider-wrapper .jcarousel-control-prev')
        .on('jcarouselcontrol:active', function() {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function() {
            $(this).addClass('inactive');
        })
        .jcarouselControl({
            // Options go here
            target: '-=1'
        });

    /*
     Next control initialization
     */
    $('.slider-wrapper .jcarousel-control-next')
        .on('jcarouselcontrol:active', function() {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function() {
            $(this).addClass('inactive');
        })
        .jcarouselControl({
            // Options go here
            target: '+=1'
        });



    var carouselNavigation = $('.small-carousel').jcarousel({
        //scroll: 1,
        //buttonPrevHTML: '<a class="nivo-prevNav">Prev</a>',
        //buttonNextHTML: '<a class="nivo-nextNav">Next</a>'
    });

    $('.slider_small .jcarousel-control-prev')
        .on('jcarouselcontrol:active', function() {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function() {
            $(this).addClass('inactive');
        })
        .jcarouselControl({
            // Options go here
            target: '-=1'
        });

    /*
     Next control initialization
     */
    $('.slider_small .jcarousel-control-next')
        .on('jcarouselcontrol:active', function() {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function() {
            $(this).addClass('inactive');
        })
        .jcarouselControl({
            // Options go here
            target: '+=1'
        });

    // We loop through the items of the navigation carousel and set it up
    // as a control for an item from the stage carousel.
    carouselNavigation.jcarousel('items').each(function() {
        var item = $(this);

        // This is where we actually connect to items.
        var target = connector(item, carouselStage);

        item
            .on('jcarouselcontrol:active', function() {
                carouselNavigation.jcarousel('scrollIntoView', this);
                item.addClass('active');
            })
            .on('jcarouselcontrol:inactive', function() {
                item.removeClass('active');
            })
            .jcarouselControl({
                target: target,
                carousel: carouselStage
            });
    });

    showToggle();

});

function showToggle()
{
    var items = $('.show_toggle > li > a');
    var allWrappers = $('.toggle');

    items.click(function(){

        var wrapper = '.'+$(this).data('item');

        $(allWrappers).hide();
        $(wrapper).toggle();

        return false;

    });
}