$(document).ready(function () {

    var button = $('a.search');
    var form = $('form.search');

    button.click(function () {

        form.submit();

        return false;

    });

    var typeSelect = $('.for_menu').get(0);

    var hiddenType = $('input[name="type"]', typeSelect);

    var select = $('ul.sub > li > a', typeSelect);

    var viewLink = $('a', typeSelect).get(0);

    select.click(function () {

        var newValue = $(this).text();
        var oldValue = $(viewLink).text();

        $(viewLink).html(newValue + "<span class='ar'></span>");
        $(this).html(oldValue);

        hiddenType.val(newValue);

        return false;

    });

});