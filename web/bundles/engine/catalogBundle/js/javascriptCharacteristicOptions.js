function characteristicOptionsTree() {

    $(document).ready(function () {

        var linkGetData = $('a.getAjaxData').attr('href');
        var linkUpdateData = $('a.updateAjaxData').attr('href');

        var setting = {
            async: {
                enable: true,
                url: linkGetData
            },
            view: {
                addDiyDom: addDiyDom,
                selectedMulti: false
            },
            edit: {
                enable: true,
                showRemoveBtn: false,
                showRenameBtn: false
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onDrop: onDrop,
                onRemove: onRemove,
                onAsyncSuccess: onAsync
            }
        };

        function onDrop(event, treeId, treeNodes, targetNode, moveType) {

            var currentNode = treeNodes[0];
            var changedNodes = null;
            var parentNode = currentNode.getParentNode();

            if (parentNode) {
                changedNodes = parentNode.children;
            } else {
                changedNodes = $.fn.zTree.getZTreeObj("tree").getNodes();
            }

            console.log(changedNodes);

            var sendData = [];

            $(changedNodes).each(function (key, value) {
                var page = {};
                page.id = value.id;
                page.pid = value.pId;
                sendData.push(page);
            });

            $.ajax({
                type: "POST",
                url: linkUpdateData,
                dataType: 'json',
                data: {data: sendData},
                success: function (data) {
                    console.log(data);
                }
            });
        }

        function onRemove(e, treeId, treeNode) {
            showLog("[ " + getTime() + " onRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
        }

        function addDiyDom(treeId, treeNode) {

            if (treeNode.editNameFlag || $("#publishBtn_" + treeNode.tId).length > 0) return;
            var sObj = $("#" + treeNode.tId + "_span");
            var classPublish = treeNode.publish == true ? 'button chk checkbox_true_full' : 'button chk checkbox_false_full';
            var publishStr = "<span class='button edit " + classPublish + "' id='publishBtn_" + treeNode.tId
                + "' title='Publish/Unpublish' onfocus='this.blur();' data-id=" + treeNode.id + "></span>";

            var editStr = "<span class='button edit' id='editBtn_" + treeNode.tId
                + "' title='Edit' onfocus='this.blur();' data-id=" + treeNode.id + "></span>";

            var deleteStr = "<span class='button remove' id='removeBtn_" + treeNode.tId
                + "' title='Remove' onfocus='this.blur();' data-id=" + treeNode.id + "></span>";

            sObj.after(deleteStr).after(editStr).after(publishStr);
            if(treeNode.required) sObj.after(requiredStr);

            $("#publishBtn_" + treeNode.tId).click(function () {

                var button = $(this);
                var link = $('.changePublishAjax').attr('href');
                var id = $(this).data('id');

                $.ajax({
                    type: "POST",
                    url: link,
                    dataType: 'json',
                    data: {id: id},
                    success: function (data) {
                        if (data.result == true) {
                            if (button.hasClass('checkbox_true_full')) {
                                button.removeClass('checkbox_true_full').addClass('checkbox_false_full');
                            } else {
                                button.removeClass('checkbox_false_full').addClass('checkbox_true_full');
                            }
                        }
                    }
                });

            });

            $("#editBtn_" + treeNode.tId).click(function () {

                document.location.href = treeNode.editLink;

            });

            $("#removeBtn_" + treeNode.tId).click(function () {

                document.location.href = treeNode.removeLink;

            });
        }

        //add to unpublished item style
        function onAsync(event, treeId, treeNode, msg) {
            $(jQuery.parseJSON(msg)).each(function (key, value) {

                var node = tree.getNodeByParam("id", value.id, null);
                var item = $('#' + node.tId + '_span');

            });

        };

        var tree = $.fn.zTree.init($("#tree"), setting);

    });

}