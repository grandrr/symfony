function characteristicTree() {

    $(document).ready(function () {

        var linkGetData = $('a.getAjaxData').attr('href');
        var linkUpdateData = $('a.updateAjaxData').attr('href');

        var setting = {
            async: {
                enable: true,
                url: linkGetData
            },
            view: {
                addDiyDom: addDiyDom,
                selectedMulti: false
            },
            edit: {
                enable: true,
                showRemoveBtn: false,
                showRenameBtn: false
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onDrop: onDrop,
                onRemove: onRemove,
                onAsyncSuccess: onAsync
            }
        };

        function onDrop(event, treeId, treeNodes, targetNode, moveType) {

            var currentNode = treeNodes[0];
            var changedNodes = null;
            var parentNode = currentNode.getParentNode();

            if (parentNode) {
                changedNodes = parentNode.children;
            } else {
                changedNodes = $.fn.zTree.getZTreeObj("tree").getNodes();
            }

            console.log(changedNodes);

            var sendData = [];

            $(changedNodes).each(function (key, value) {
                var page = {};
                page.id = value.id;
                page.pid = value.pId;
                sendData.push(page);
            });

            $.ajax({
                type: "POST",
                url: linkUpdateData,
                dataType: 'json',
                data: {data: sendData},
                success: function (data) {
                    console.log(data);
                }
            });
        }

        function onRemove(e, treeId, treeNode) {
            showLog("[ " + getTime() + " onRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
        }

        function addDiyDom(treeId, treeNode) {

            if (treeNode.editNameFlag || $("#publishBtn_" + treeNode.tId).length > 0) return;
            var sObj = $("#" + treeNode.tId + "_span");
            var classPublish = treeNode.publish == true ? 'button chk checkbox_true_full' : 'button chk checkbox_false_full';
            var requiredStr = "<span class='button required' title='Publish/Unpublish' data-id=" + treeNode.id + "></span>";
            var publishStr = "<span class='button edit " + classPublish + "' id='publishBtn_" + treeNode.tId
                + "' title='Publish/Unpublish' onfocus='this.blur();' data-id=" + treeNode.id + "></span>";

            var editStr = "<span class='button edit' id='editBtn_" + treeNode.tId
                + "' title='Edit' onfocus='this.blur();' data-id=" + treeNode.id + "></span>";

            var deleteStr = "<span class='button remove' id='removeBtn_" + treeNode.tId
                + "' title='Remove' onfocus='this.blur();' data-id=" + treeNode.id + "></span>";

            sObj.after(deleteStr).after(editStr).after(publishStr);
            if (treeNode.required) sObj.after(requiredStr);

            if (treeNode.option) {
                var optionStr = "<span class='button option' id='optionBtn_" + treeNode.tId
                    + "' title='View options' onfocus='this.blur();' data-id=" + treeNode.id + "></span>";
                sObj.after(optionStr);
            }

            $("#optionBtn_" + treeNode.tId).click(function () {
                document.location.href = treeNode.option;
            });

            $("#publishBtn_" + treeNode.tId).click(function () {

                var button = $(this);
                var link = $('.changePublishAjax').attr('href');
                var id = $(this).data('id');

                $.ajax({
                    type: "POST",
                    url: link,
                    dataType: 'json',
                    data: {id: id},
                    success: function (data) {
                        if (data.result == true) {
                            if (button.hasClass('checkbox_true_full')) {
                                button.removeClass('checkbox_true_full').addClass('checkbox_false_full');
                            } else {
                                button.removeClass('checkbox_false_full').addClass('checkbox_true_full');
                            }
                        }
                    }
                });

            });

            $("#editBtn_" + treeNode.tId).click(function () {

                document.location.href = treeNode.editLink;

            });

            $("#removeBtn_" + treeNode.tId).click(function () {

                document.location.href = treeNode.removeLink;

            });

//        var btn = $("#diyBtn_" + treeNode.id);
//        if (btn) btn.bind("click", function () {
//            alert("diy Button for " + treeNode.name);
//        });
//        if (treeNode.id == 1) {
//            var editStr = "<span class='demoIcon' id='diyBtn_" + treeNode.id + "' title='" + treeNode.name + "' onfocus='this.blur();'><span class='button icon01'></span></span>";
//            aObj.append(editStr);
//            var btn = $("#diyBtn_" + treeNode.id);
//            if (btn) btn.bind("click", function () {
//                alert("diy Button for " + treeNode.name);
//            });
//        } else if (treeNode.id == 22) {
//            var editStr = "<span class='demoIcon' id='diyBtn_" + treeNode.id + "' title='" + treeNode.name + "' onfocus='this.blur();'><span class='button icon02'></span></span>";
//            aObj.after(editStr);
//            var btn = $("#diyBtn_" + treeNode.id);
//            if (btn) btn.bind("click", function () {
//                alert("diy Button for " + treeNode.name);
//            });
//        } else if (treeNode.id == 23) {
//            var editStr = "<select class='selDemo' id='diyBtn_" + treeNode.id + "'><option value=1>1</option><option value=2>2</option><option value=3>3</option></select>";
//            aObj.after(editStr);
//            var btn = $("#diyBtn_" + treeNode.id);
//            if (btn) btn.bind("change", function () {
//                alert("diy Select value=" + btn.attr("value") + " for " + treeNode.name);
//            });
//        } else if (treeNode.id == 24) {
//            var editStr = "<span id='diyBtn_" + treeNode.id + "'>Text Demo...</span>";
//            aObj.after(editStr);
//        } else if (treeNode.id == 25) {
//            var editStr = "<a id='diyBtn1_" + treeNode.id + "' onclick='alert(1);return false;'>链接1</a>" +
//                "<a id='diyBtn2_" + treeNode.id + "' onclick='alert(2);return false;'>链接2</a>";
//            aObj.after(editStr);
//        }
        }

        //add to unpublished item style
        function onAsync(event, treeId, treeNode, msg) {
            $(jQuery.parseJSON(msg)).each(function (key, value) {

                var node = tree.getNodeByParam("id", value.id, null);
                var item = $('#' + node.tId + '_span');

            });

        };

        var tree = $.fn.zTree.init($("#tree"), setting);

    });

}

function pageEdit() {

    $(document).ready(function () {

        _tiny();

    });

}

function _tiny() {
    tinymce.init({
        selector: "textarea.tiny",
        theme: "modern",
        width: 'auto',
        height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        content_css: "css/content.css",
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });
}

function characteristicTiny() {

    $(document).ready(function () {

        _tiny();

    });

}

function characteristicFiles() {

    $(document).ready(function () {

        var filesEl = $("[html5upload='html5upload']");

        filesEl.each(function(key, value){

            var wrapper = $(value).parent('div');
            $(value).after('<input class="btn btn-small" type="button" value="Upload file">');
            var button = $('.btn.btn-small', wrapper);
            var uploadUrl = $('a.uploadUrl', wrapper).attr('href');

            button.click(function(){

                if(uploadUrl){
                uploadFile(value, uploadUrl)
                }else{
                    alert('First save data!');
                }

            });

        })

    });

}

function characteristicMultifile() {

    $(document).ready(function () {

        var filesEl = $("[html5multiupload='html5multiupload']");

        filesEl.each(function(key, value){

            var wrapper = $(value).parent('div');
            $(value).after('<input class="btn btn-small" type="button" value="Upload file">');
            var button = $('.btn.btn-small', wrapper);
            var uploadUrl = $('a.multiuploadUrl', wrapper).attr('href');

            button.click(function(){

                if(uploadUrl){
                    uploadFiles(value, uploadUrl)
                }else{
                    alert('First save data!');
                }

            });

        })

        var deleteFile = $('a.ajaxRemoveFile');

        deleteFile.click(function(){

            var link = $(this).attr('href');
            var removeWrapper = $(this).parent('div').parent('div');

            $.ajax({
                url: link,
                dataType: 'json'
            }).success(function(data) {
//                console.log(data);
                removeWrapper.remove();
            });

            return false;

        });

    });

}