<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/1/14
 * Time: 3:58 PM
 */

namespace Engine\CatalogBundle\Twig\Extension;

use Engine\CatalogBundle\Services\CharacteristicFinder;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Engine\CatalogBundle\Entity\Product\Product;


class CatalogBundleExtension extends \Twig_Extension
{

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $characteristicRepository;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    protected $router;

    protected $numberOptions = array(
        'height' => array(
            'min' => 160,
            'max' => 195,
            'step' => 5
        ),
        'bust' => array(
            'min' => 10,
            'max' => 30,
            'step' => 2
        ),
        'waist' => array(
            'min' => 1,
            'max' => 10,
            'step' => 2
        ),
        'hips' => array(
            'min' => 10,
            'max' => 20,
            'step' => 2
        ),
    );

    protected $optionsByCategory = array(
        'man'=> array('height'=>'height', 'waist'=>'waist', 'hips'=>'hips', 'hair'=>'hair', 'location'=>'location'),
        'woman'=> array('height'=>'height', 'waist'=>'waist', 'hips'=>'hips', 'hair'=>'hair', 'location'=>'location'),
        'talents'=> array('gender'=>'gender', 'type_talents'=>'type', 'location'=>'location'),
        'artists'=> array('gender'=>'gender', 'type_artists'=>'type', 'location'=>'location'),
    );

    protected $viewOptionsByType = array(
        'man'=> array('height', 'waist', 'hips', 'hair', 'location'),
        'woman'=> array('height', 'waist', 'hips', 'hair', 'location'),
        'talents'=> array('gender', 'type_talents', 'location'),
        'artists'=> array('gender', 'type_artists', 'location'),
    );

    /**
     * @param CharacteristicFinder $characteristicFinder
     * @param Router $router
     */
    public function __construct(CharacteristicFinder $characteristicFinder, Router $router)
    {
        $this->characteristicFinder = $characteristicFinder;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            'searchOptions' => new \Twig_Function_Method($this, 'searchOptions'),
            'viewOptions' => new \Twig_Function_Method($this, 'viewOptions'),
            'isInteger' => new \Twig_Function_Method($this, 'isInteger'),
        );
    }

    public function searchOptions($request, $category)
    {

        if(!isset($this->optionsByCategory[$category])){
            return false;
        }

        $characteristicList = $this->optionsByCategory[$category];

//        $this->router->generate("engine_catalog_index_search")

        $ulCont = '<form class="search" method="POST"><ul class="menu_hb clearfix">%s</ul></form>';

        $liCont = '<li class="for_menu"><a href="#">%s<span class="ar"></span></a><ul class="sub">%s</ul></li>';

        $items = '';

        foreach ($characteristicList as $systemName => $characteristic) {

            $characteristicEntity = $this->characteristicFinder->getCharacteristicBySystemName($systemName);

            $li = '';

            if(isset($this->numberOptions[$characteristicEntity->getSystemName()])){

                $options = $this->getOptions($characteristicEntity);

                foreach($options as $option){

                    $checked = in_array($option['value'], $request) ? 'checked': '';

                    $li .= '<li class="sub"><div class="sub">
                        <input name="' . $option['value'].
                        '" type="checkbox" '.$checked.'/>
                        ' .$option['viewName']. '
                        </div></li>';
                }

            }

            if ($characteristicEntity->getType() == 'choice') {

                foreach ($characteristicEntity->getCharacteristicOption() as $option) {

                    if ($option->getPublish()) {

                        $name = $characteristicEntity->getId() . '_' . $option->getId();

                        $checked = in_array($name, $request) ? 'checked' : '';

                        $li .= '<li class="sub"><div class="sub">
                        <input name="' . $name . '" type="checkbox" ' . $checked . '/>
                        ' . $option->getViewName() . '
                        </div></li>';

                    }

                }

            }

            $items .= sprintf($liCont, $characteristic, $li);

        }

        $items .= '<li class="margin_r for_search">
        <a href="" class="search">search</a>
        </li>';

        $return = sprintf($ulCont, $items);

        echo $return;

    }

    /**
     * @param $characteristic
     * @return array
     */
    protected function getOptions($characteristic)
    {

        $inputsData = array();

        if (isset($this->numberOptions[$characteristic->getSystemName()])) {

            $options = $this->numberOptions[$characteristic->getSystemName()];

            $oneFoot = 0.032;

            for ($i = $options['min']; $i <= $options['max']; $i = $i + $options['step']) {

                $footFrom = $i * $oneFoot;
                $cmFrom = $i;
                $footTo = ($i + $options['step']) * $oneFoot;
                $cmTo = $i + $options['step'];

//                $inputsData[] = array('viewName' => $footFrom . '"/ ' . $cmFrom . '-' . $footTo . '/' . $cmTo, 'value' => $characteristic->getId() . '_' . $cmFrom . '_' . $cmTo);
                $inputsData[] = array('viewName' => $cmFrom . ' cm - ' . $cmTo . ' cm', 'value' => $characteristic->getId() . '_' . $cmFrom . '_' . $cmTo);

            }

        }

        return $inputsData;

    }

    public function viewOptions(Product $product)
    {

        $currentType = $product->getCharacteristicValueBySystemName('type');

        if ($currentType) {

            $result = '';
            $viewOptions = array();

            $currentType = $currentType->getValue()->getTitle();

            $currentViewOptions = $this->viewOptionsByType[$currentType];

            foreach ($currentViewOptions as $viewOption) {
                $characteristicValue = $product->getCharacteristicValueBySystemName($viewOption);

                $viewName = $characteristicValue->getCharacteristic()->getSystemName();
                $value = $characteristicValue->getValue();

                if (is_object($value)) {
                    $value = $value->getViewName();
                }

                $viewOptions[] = '<li><span>' . $viewName . ': </span>' . $value . '</li>';
            }

            $result = implode('<li>/</li>', $viewOptions);

            return $result;

        }

    }

    public function isInteger($number)
    {
        return floor($number) == $number ? true : false;
    }

    public function getName()
    {
        return 'catalog_extension';
    }

} 