<?php

namespace Engine\CatalogBundle\Entity\Characteristic;

use Doctrine\ORM\Mapping as ORM;
use Engine\FileBundle\Entity\File;

/**
 * Characteristic
 *
 * @ORM\Table("characteristics_values")
 * @ORM\Entity(repositoryClass="Engine\CatalogBundle\Entity\Characteristic\CharacteristicValueRepository")
 */
class CharacteristicValue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="checkbox", type="boolean")
     */
    private $checkbox;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @ORM\OneToOne(targetEntity="Engine\CatalogBundle\Entity\Characteristic\CharacteristicOption")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $choice;

    /**
     * @ORM\OneToOne(targetEntity="Engine\FileBundle\Entity\File")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity="Engine\CatalogBundle\Entity\Characteristic\Characteristic")
     * @ORM\JoinColumn(name="characteristic_id", referencedColumnName="id")
     */
    private $characteristic;

    /**
     * @ORM\ManyToOne(targetEntity="Engine\CatalogBundle\Entity\Product\Product")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    private $entity;

    /**
     * @ORM\OneToMany(targetEntity="Engine\FileBundle\Entity\File",
    mappedBy="characteristicValue", cascade={"persist", "remove"})
     */
    private $multifile;

    function __construct(
//        Product $entity,
//        Characteristic $characteristic,
//        $type,
//        $checkbox = null,
//        $numeric = null,
//        $option = null,
//        $text = null
        $entity,
        $characteristic,
        $type,
        $checkbox = null,
        $number = null,
        $choice = null,
        $text = null,
        $file = null
    )
    {
        $this->setEntity($entity);
        $this->setCharacteristic($characteristic);
        $this->setType($type);
        $this->setCheckbox($checkbox);
        $this->setNumber($number);
        $this->setChoice($choice);
        $this->setFile($file);
        $this->setText($text);

//        $this->characteristic = $characteristic;
//        $this->checkbox = $checkbox;
//        $this->entity = $entity;
//        $this->numeric = $numeric;
//        $this->option = $option;
//        $this->text = $text;
//        $this->type = $type;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $characteristic
     */
    public function setCharacteristic($characteristic)
    {
        $this->characteristic = $characteristic;
    }

    /**
     * @return int
     */
    public function getCharacteristic()
    {
        return $this->characteristic;
    }

    /**
     * @param int $checkbox
     */
    public function setCheckbox($checkbox)
    {
        $this->checkbox = $checkbox;
    }

    /**
     * @return int
     */
    public function getCheckbox()
    {
        return $this->checkbox;
    }

    /**
     * @param int $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getTextarea()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setTextarea($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getTinymce()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setTinymce($text)
    {
        $this->text = $text;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param CharacteristicOption $choice
     * @return $this
     */
    public function setChoice(\Engine\CatalogBundle\Entity\Characteristic\CharacteristicOption $choice = null)
    {
        $this->choice = $choice;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChoice()
    {
        return $this->choice;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $function = 'set' . $this->getType();
        $this->$function($value);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        $function = 'get' . $this->getType();
        return $this->$function();
    }

    public function setMultifile()
    {

    }

    /**
     * @param File $multifile
     * @return $this
     */
    public function addMultifile(\Engine\FileBundle\Entity\File $multifile)
    {
        $this->multifile[] = $multifile;

        return $this;
    }

    /**
     * @param File $multifile
     */
    public function removeMultifile(\Engine\FileBundle\Entity\File $multifile)
    {
        $this->multifile->removeElement($multifile);
    }

    /**
     * @return mixed
     */
    public function getMultifile()
    {
        return $this->multifile;
    }
}
