<?php

namespace Engine\CatalogBundle\Entity\Characteristic;

use Engine\EngineBundle\Entity\EntityFactoryInterface;
use Doctrine\ORM\EntityRepository;

class CharacteristicOptionFactory implements EntityFactoryInterface
{

    /**
     * @var
     */
    protected $characteristicRepository;

    /**
     * @param EntityRepository $characteristicRepository
     */
    function __construct(EntityRepository $characteristicRepository)
    {
        $this->characteristicRepository = $characteristicRepository;
    }


    /**
     * @param array $data
     * @return Characteristic
     */
    public function create(array $data = null)
    {
        $characteristic = $this->characteristicRepository->find($data['characteristicId']);

        $entity = new CharacteristicOption(
            $data['title'],
            $data['viewName'],
            $characteristic,
            $data['publish']
        );

        return $entity;
    }
}
