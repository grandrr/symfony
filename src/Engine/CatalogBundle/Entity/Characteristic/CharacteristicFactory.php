<?php

namespace Engine\CatalogBundle\Entity\Characteristic;

use Engine\EngineBundle\Entity\EntityFactoryInterface;

class CharacteristicFactory implements EntityFactoryInterface
{
    /**
     * @param array $data
     * @return Characteristic
     */
    public function create(array $data = null)
    {
        $entity = new Characteristic(
            $data['systemName'],
            $data['type'],
            $data['required'],
            $data['system'],
            $data['publish']
        );

        return $entity;
    }
}
