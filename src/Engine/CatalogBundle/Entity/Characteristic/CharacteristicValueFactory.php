<?php

namespace Engine\CatalogBundle\Entity\Characteristic;

use Engine\EngineBundle\Entity\EntityFactoryInterface;
use Doctrine\ORM\EntityRepository;

class CharacteristicValueFactory implements EntityFactoryInterface
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $productRepository;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $characteristicRepository;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $characteristicOptionRepository;

    /**
     * @param EntityRepository $productRepository
     * @param EntityRepository $characteristicRepository
     * @param EntityRepository $characteristicOptionRepository
     */
    public function __construct(
        EntityRepository $productRepository,
        EntityRepository $characteristicRepository,
        EntityRepository $characteristicOptionRepository
    )
    {
        $this->productRepository = $productRepository;
        $this->characteristicRepository = $characteristicRepository;
        $this->characteristicOptionRepository = $characteristicOptionRepository;
    }

    /**
     * @param array $data
     * @return CharacteristicValue
     */
    public function create(array $data = null)
    {

        $product = $this->productRepository->find($data['productId']);
        $characteristic = $this->characteristicRepository->find($data['characteristicId']);

        switch ($data['characteristicType']) {
            case Characteristic::textareaType:
            case Characteristic::textType:
            case Characteristic::tinymceType:
                $entity = new CharacteristicValue(
                    $product,
                    $characteristic,
                    $data['characteristicType'],
                    null,
                    null,
                    null,
                    $data['characteristicValue'],
                    null
                );

                break;

            case Characteristic::checkboxType:
                $entity = new CharacteristicValue(
                    $product,
                    $characteristic,
                    $data['characteristicType'],
                    $data['characteristicValue'],
                    null,
                    null,
                    null,
                    null
                );
                break;

            case Characteristic::numberType:
                $entity = new CharacteristicValue(
                    $product,
                    $characteristic,
                    $data['characteristicType'],
                    null,
                    $data['characteristicValue'],
                    null,
                    null,
                    null
                );
                break;

            case Characteristic::selectType:
                $option = $this->characteristicOptionRepository->find($data['characteristicValue']);
                $entity = new CharacteristicValue(
                    $product,
                    $characteristic,
                    $data['characteristicType'],
                    null,
                    null,
                    $option,
                    null,
                    null
                );
                break;

            case Characteristic::fileType:
                $entity = new CharacteristicValue(
                    $product,
                    $characteristic,
                    $data['characteristicType'],
                    null,
                    null,
                    null,
                    null,
                    $data['characteristicValue']
                );

            case Characteristic::multiFileType:
                $entity = new CharacteristicValue(
                    $product,
                    $characteristic,
                    $data['characteristicType'],
                    null,
                    null,
                    null,
                    null,
                    null
                );

                break;
        }

        return $entity;
    }
}
