<?php

namespace Engine\CatalogBundle\Entity\Characteristic;

use Doctrine\ORM\Mapping as ORM;

/**
 * CharacteristicOption
 *
 * @ORM\Table("characteristics_options")
 * @ORM\Entity(repositoryClass="Engine\CatalogBundle\Entity\Characteristic\CharacteristicOptionRepository")
 */
class CharacteristicOption
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="view_name", type="string", length=255)
     */
    private $viewName;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="publish", type="boolean")
     */
    private $publish;

    /**
     * @ORM\OneToOne(targetEntity="Engine\CatalogBundle\Entity\Characteristic\Characteristic")
     * @ORM\JoinColumn(name="characteristic_id", referencedColumnName="id")
     */
    private $characteristic;

    function __construct(
        $title,
        $viewName,
        Characteristic $characteristic,
        $publish
    )
    {
        $this->setTitle($title);
        $this->setViewName($viewName);
        $this->setCharacteristic($characteristic);
        $this->setPublish($publish);
        $this->setPosition(0);
    }

    /**
     * @param mixed $characteristic
     */
    public function setCharacteristic(Characteristic $characteristic)
    {
        $this->characteristic = $characteristic;
    }

    /**
     * @return mixed
     */
    public function getCharacteristic()
    {
        return $this->characteristic;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CharacteristicOption
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $viewName
     */
    public function setViewName($viewName)
    {
        $this->viewName = $viewName;
    }

    /**
     * @return string
     */
    public function getViewName()
    {
        return $this->viewName;
    }

    /**
     * Set position
     *
     * @param boolean $position
     * @return CharacteristicOption
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return boolean
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set publish
     *
     * @param integer $publish
     * @return CharacteristicOption
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return integer
     */
    public function getPublish()
    {
        return $this->publish;
    }
}
