<?php

namespace Engine\CatalogBundle\Entity\Characteristic;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Characteristic
 *
 * @ORM\Table("characteristics")
 * @ORM\Entity(repositoryClass="Engine\CatalogBundle\Entity\Characteristic\CharacteristicRepository")
 */
class Characteristic
{

    CONST checkboxType = 'checkbox';
    CONST numberType = 'number';
    CONST textType = 'text';
    CONST textareaType = 'textarea';
    CONST tinymceType = 'tinymce';
    CONST selectType = 'choice';
    CONST fileType = 'file';
    CONST multiFileType = 'multifile';

    STATIC $availableTypes = array(
        self::checkboxType => self::checkboxType,
        self::numberType => self::numberType,
        self::textType => self::textType,
        self::textareaType => self::textareaType,
        self::tinymceType => self::tinymceType,
        self::selectType => self::selectType,
        self::fileType => self::fileType,
        self::multiFileType => self::multiFileType,
    );

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var text
     *
     * @ORM\Column(name="systemName", type="string", length=255)
     */
    private $systemName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="required", type="boolean")
     */
    private $required;

    /**
     * @var boolean
     *
     * @ORM\Column(name="system", type="boolean")
     */
    private $system;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="publish", type="boolean")
     */
    private $publish;

    /**
     * @ORM\OneToMany(targetEntity="Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue",
    mappedBy="characteristic", cascade={"persist", "remove"})
     */
    private $characteristicValue;

    /**
     * @ORM\OneToMany(targetEntity="Engine\CatalogBundle\Entity\Characteristic\CharacteristicOption",
    mappedBy="characteristic", cascade={"persist", "remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $characteristicOption;

    function __construct(
        $systemName,
        $type,
        $required,
        $sustem,
        $publish
    )
    {
        $this->setSystemName($systemName);
        $this->setType($type);
        $this->setRequired($required);
        $this->setSystem($sustem);
        $this->setPublish($publish);
        $this->setPosition(0);
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param boolean $required
     */
    public function setRequired($required)
    {
        $this->required = $required;
    }

    /**
     * @return boolean
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * @param boolean $system
     */
    public function setSystem($system)
    {
        $this->system = $system;
    }

    /**
     * @return boolean
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * @param \Engine\CatalogBundle\Entity\Characteristic\text $systemName
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;
    }

    /**
     * @return \Engine\CatalogBundle\Entity\Characteristic\text
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $publish
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;
    }

    /**
     * @return int
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Characteristic
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Add characteristicValue
     *
     * @param \Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue $characteristicValue
     * @return Characteristic
     */
    public function addCharacteristicValue(\Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue $characteristicValue)
    {
        $this->characteristicValue[] = $characteristicValue;

        return $this;
    }

    /**
     * Remove characteristicValue
     *
     * @param \Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue $characteristicValue
     */
    public function removeCharacteristicValue(\Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue $characteristicValue)
    {
        $this->characteristicValue->removeElement($characteristicValue);
    }

    /**
     * Get characteristicValue
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharacteristicValue()
    {
        return $this->characteristicValue;
    }

    /**
     * Add characteristicOption
     *
     * @param \Engine\CatalogBundle\Entity\Characteristic\CharacteristicOption $characteristicOption
     * @return Characteristic
     */
    public function addCharacteristicOption(\Engine\CatalogBundle\Entity\Characteristic\CharacteristicOption $characteristicOption)
    {
        $this->characteristicOption[] = $characteristicOption;

        return $this;
    }

    /**
     * Remove characteristicOption
     *
     * @param \Engine\CatalogBundle\Entity\Characteristic\CharacteristicOption $characteristicOption
     */
    public function removeCharacteristicOption(\Engine\CatalogBundle\Entity\Characteristic\CharacteristicOption $characteristicOption)
    {
        $this->characteristicOption->removeElement($characteristicOption);
    }

    /**
     * Get characteristicOption
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharacteristicOption()
    {
        return $this->characteristicOption;
    }
}
