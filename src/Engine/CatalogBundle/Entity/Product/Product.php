<?php

namespace Engine\CatalogBundle\Entity\Product;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * Product
 *
 * @ORM\Table("products")
 * @ORM\Entity(repositoryClass="Engine\CatalogBundle\Entity\Product\ProductRepository")
 * @GRID\Source(columns="id, alias")
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @GRID\Column(size="5", filterable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255)
     * @GRID\Column(filterable=false)
     */
    private $alias;

    /**
     * @var integer
     *
     * @ORM\Column(name="publish", type="boolean")
     */
    private $publish;

    /**
     * @ORM\OneToMany(targetEntity="Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue",
     * mappedBy="entity", cascade={"persist", "remove"})
     */
    private $characteristicValue;

    /**
     * @param $alias
     * @param $publish
     */
    function __construct($alias, $publish)
    {
        $this->setAlias($alias);
        $this->setPublish($publish);
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Product
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set publish
     *
     * @param integer $publish
     * @return Product
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return integer 
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Add characteristicValue
     *
     * @param \Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue $characteristicValue
     * @return Product
     */
    public function addCharacteristicValue(\Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue $characteristicValue)
    {
        $this->characteristicValue[] = $characteristicValue;

        return $this;
    }

    /**
     * Remove characteristicValue
     *
     * @param \Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue $characteristicValue
     */
    public function removeCharacteristicValue(\Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue $characteristicValue)
    {
        $this->characteristicValue->removeElement($characteristicValue);
    }

    /**
     * Get characteristicValue
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCharacteristicValue()
    {
        return $this->characteristicValue;
    }

    public function getCharacteristicValueBySystemName($systemName)
    {
        foreach($this->getCharacteristicValue() as $characteristicValue)
        {
            if($characteristicValue->getCharacteristic()->getSystemName() == (string)$systemName)
                return $characteristicValue;
        }
    }
}
