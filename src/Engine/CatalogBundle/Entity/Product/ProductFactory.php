<?php

namespace Engine\CatalogBundle\Entity\Product;

use Engine\EngineBundle\Entity\EntityFactoryInterface;

class ProductFactory implements EntityFactoryInterface
{
    public function create(array $data = null)
    {
        $entity = new Product(
            $data['alias'],
            $data['publish']
        );

        return $entity;
    }
}
