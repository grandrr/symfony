<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/6/14
 * Time: 1:49 PM
 */

namespace Engine\CatalogBundle\Forms\CharacteristicValue;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Engine\CatalogBundle\Services\CharacteristicFinder;
use Engine\CatalogBundle\Entity\Characteristic\Characteristic;

class Create extends AbstractType
{
    /**
     * @var \Engine\CatalogBundle\Services\CharacteristicFinder
     */
    protected $characteristicFinder;

    /**
     * @param CharacteristicFinder $characteristicFinder
     */
    public function __construct(CharacteristicFinder $characteristicFinder)
    {
        $this->characteristicFinder = $characteristicFinder;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            //disable html5 validation
            'attr' => array('novalidate' => 'novalidate'),
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention' => 'page_item',
        ));
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        foreach ($this->characteristicFinder->getCharacteristicsForDashboard() as $characteristic) {

            if (!in_array($characteristic->getType(), Characteristic::$availableTypes))
                throw new \Exception('Undefined field type!');

            $constraints = array();

            if ($characteristic->getRequired()) array_push($constraints, new NotBlank());

            switch ($characteristic->getType()) {

                case Characteristic::tinymceType:
                    $builder
                        ->add($characteristic->getId() . '_' . $characteristic->getType(), 'textarea', array(
                            'label' => $characteristic->getSystemName(),
                            'constraints' => $constraints,
                            'attr' => array(
                                'class' => 'tiny span6',
                            ),
                        ));
                    break;

                case Characteristic::selectType:
                    $choices = array();
                    foreach ($characteristic->getCharacteristicOption() as $characteristicOption) {
                        $choices[$characteristicOption->getId()] = $characteristicOption->getViewName();
                    }
                    $builder
                        ->add($characteristic->getId() . '_' . $characteristic->getType(), $characteristic->getType(), array(
                            'label' => $characteristic->getSystemName(),
                            'constraints' => $constraints,
                            'choices' => $choices,
                        ));
                    break;

                case Characteristic::fileType:
                    $builder
                        ->add($characteristic->getId() . '_' . $characteristic->getType(), $characteristic->getType(), array(
                            'label' => $characteristic->getSystemName(),
                            'required' => false,
                            'empty_data'  => null,
//                            'data_class'  => null,
                            'attr' => array(
                                'html5upload' => true,
                            ),
                        ));
                    $builder
                        ->add($characteristic->getId() . '_' . $characteristic->getType() . '_hidden', 'hidden', array(
                            'required' => false,
                            'empty_data'  => null,
                        ));
                    break;

                case Characteristic::multiFileType:
                    $builder
                        ->add($characteristic->getId() . '_' . $characteristic->getType(), 'file', array(
                            'label' => $characteristic->getSystemName(),
                            'multiple' => true,
                            'required' => false,
                            'empty_data'  => null,
//                            'data_class'  => null,
                            'attr' => array(
                                'html5multiupload' => true,
                            ),
                        ));
                    $builder
                        ->add($characteristic->getId() . '_' . $characteristic->getType() . '_hidden', 'hidden', array(
                            'required' => false,
                            'empty_data'  => null,
                        ));
                    break;

                default:
                    $builder
                        ->add($characteristic->getId() . '_' . $characteristic->getType(), $characteristic->getType(), array(
                            'label' => $characteristic->getSystemName(),
                            'constraints' => $constraints,
                            'attr' => array(
                                'class' => $characteristic->getType() != 'checkbox' ? 'span6' : '',
                            ),
                        ));
                    break;
            }
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'characteristicValue';
    }
}