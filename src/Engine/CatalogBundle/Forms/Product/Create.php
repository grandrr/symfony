<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/6/14
 * Time: 1:49 PM
 */

namespace Engine\CatalogBundle\Forms\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

use Engine\CatalogBundle\Forms\CharacteristicValue\Create as CreateCharacteristic;

class Create extends AbstractType
{
    /**
     * @var \Engine\CatalogBundle\Forms\CharacteristicValue\Create
     */
    protected $characteristicForm;

    /**
     * @param CreateCharacteristic $characteristicForm
     */
    public function __construct(CreateCharacteristic $characteristicForm){

        $this->characteristicForm = $characteristicForm;

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            //disable html5 validation
            'attr' => array('novalidate' => 'novalidate'),
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention' => 'page_item',
        ));
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('alias', 'text', array(
                'label' => 'alias',
                'constraints' => array(
                    new NotBlank(),
                ),
            ))
            ->add('publish', 'checkbox', array(
                'label' => 'Publish',
            ))
            ->add('save', 'submit');

        $builder->add('characteristics', $this->characteristicForm);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'product';
    }
}