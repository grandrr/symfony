<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/6/14
 * Time: 1:49 PM
 */

namespace Engine\CatalogBundle\Forms\Characteristic;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Engine\CatalogBundle\Entity\Characteristic\Characteristic;

class Create extends AbstractType
{
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            //disable html5 validation
            'attr' => array('novalidate' => 'novalidate'),
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention' => 'page_item',
        ));
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('systemName', 'text', array(
                'label' => 'System Name',
                'required' => true
//                'constraints' => array(
//                    new NotBlank(),
//                )
            ))
            ->add('type', 'choice', array(
                'choices' => Characteristic::$availableTypes,
                'required' => true,
            ))
            ->add('required', 'checkbox', array(
                'label' => 'Reuqired',
            ))
            ->add('system', 'checkbox', array(
                'label' => 'System',
            ))
            ->add('publish', 'checkbox', array(
                'label' => 'Publish',
            ))
            ->add('save', 'submit');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'characteristic';
    }
}