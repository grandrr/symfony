<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/6/14
 * Time: 1:49 PM
 */

namespace Engine\CatalogBundle\Forms\CharacteristicOption;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class Edit extends Create
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('id', 'hidden', array(
            'constraints' => array(
                new NotBlank(),
            ),
        ));
    }
}