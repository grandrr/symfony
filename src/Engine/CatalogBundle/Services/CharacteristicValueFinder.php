<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\CatalogBundle\Services;

use Doctrine\ORM\EntityRepository;

/**
 * Class CategoryFinder
 * @package Acme\BlogBundle\Services
 */
class CharacteristicValueFinder
{
    /**
     * @var
     */
    protected $characteristicValueRepository;

    /**
     * @var
     */
    protected $productRepository;

    /**
     * @var
     */
    protected $characteristicRepository;

    /**
     * @param EntityRepository $characteristicValueRepository
     * @param EntityRepository $productRepository
     * @param EntityRepository $characteristicRepository
     */
    public function __construct(
        EntityRepository $characteristicValueRepository,
        EntityRepository $productRepository,
        EntityRepository $characteristicRepository
    )
    {
        $this->characteristicValueRepository = $characteristicValueRepository;
        $this->productRepository = $productRepository;
        $this->characteristicRepository = $characteristicRepository;
    }

    /**
     * @param $entityId
     * @param $characteristicId
     * @return null|object
     */
    public function getCharacteristicValueForProductFormDashboard($entityId, $characteristicId)
    {
        $entity = $this->productRepository->find($entityId);
        $characteristic = $this->characteristicRepository->find($characteristicId);

        return $this->characteristicValueRepository->findOneBy(array(
            'entity' => $entity,
            'characteristic' => $characteristic
        ));
    }
}