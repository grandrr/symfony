<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\CatalogBundle\Services;

use Engine\EngineBundle\Services\AbstractEntityManager;

/**
 * Class ProductManager
 * @package Acme\BlogBundle\Services
 */
class CharacteristicOptionsManager extends AbstractEntityManager
{
    public function updatePosition(array $data)
    {
        $position = 0;
        foreach ($data as $elem) {
            $entity = $this->entityRepository->find($elem['id']);
            $entity->setPosition($position);
            $this->saveEntity($entity);
            $position++;
        }
    }

    public function changePublish($id)
    {
        $entity = $this->entityRepository->find($id);
        if ($entity->getPublish()) {
            $entity->setPublish(false);
        } else {
            $entity->setPublish(true);
        }
        $this->saveEntity($entity);
        return true;
    }
} 