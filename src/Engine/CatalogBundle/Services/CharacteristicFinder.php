<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\CatalogBundle\Services;

use Doctrine\ORM\EntityRepository;
use Engine\CatalogBundle\Entity\Characteristic\Characteristic;

/**
 * Class CategoryFinder
 * @package Acme\BlogBundle\Services
 */
class CharacteristicFinder
{
    /**
     * @var \Engine\CatalogBundle\Entity\Characteristic\CharacteristicRepository
     */
    protected $characteristicRepository;

    /**
     * @var \Router
     */
    protected $router;

    /**
     * @param EntityRepository $characteristicRepository
     * @param $router
     */
    public function __construct(
        EntityRepository $characteristicRepository,
        $router
    )
    {
        $this->characteristicRepository = $characteristicRepository;
        $this->router = $router;
    }

    /**
     * @param $systemName
     * @return null|object
     */
    public function getCharacteristicBySystemName($systemName)
    {
        return $this->characteristicRepository->findOneBy(array(
            'systemName' => (string)$systemName,
        ));
    }

    public function getCharacteristicsForDashboard()
    {
        return $this->characteristicRepository->findBy(array( //'publish' => true,
        ), array(
            'position' => 'ASC',
        ));
    }

    /**
     * @return array|null
     */
    public function getCharacteristicsJsonForDashboard()
    {
        $result = array();

        $entitys = $this->characteristicRepository->findBy(array(), array(
            'position' => 'ASC',
        ));

        if ($entitys) {
            foreach ($entitys as $key => $entity) {
                $result = $this->getEntitys($entity, $result);
            }
        }
        return $result;
    }

    /**
     * @param $entity
     * @param null $result
     * @return array|null
     */
    protected function getEntitys($entity, $result = null)
    {
        $data['id'] = $entity->getId();
        $data['pId'] = null;
        $data['name'] = $entity->getSystemName();
        $data['publish'] = $entity->getPublish();
        $data['required'] = $entity->getRequired();
        $data['option'] = $entity->getType() == Characteristic::selectType ?
            $data['viewOptions'] = $this->router->generate('engine_characteristic_options_dashboard',
                array('id' => $entity->getId())
            ) :
            false;
        $data['editLink'] = $this->router->generate('engine_characteristic_dashboard_edit', array('id' => $entity->getId()));
        $data['removeLink'] = $this->router->generate('engine_characteristic_dashboard_remove', array('id' => $entity->getId()));
        $data['open'] = true;

        $result[] = $data;

        return $result;
    }
}