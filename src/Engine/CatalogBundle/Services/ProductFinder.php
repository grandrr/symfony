<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\CatalogBundle\Services;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\EntityManager;
use Engine\CatalogBundle\Entity\Characteristic\Characteristic;

/**
 * Class CategoryFinder
 * @package Acme\BlogBundle\Services
 */
class ProductFinder
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $productRepository;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $characteristicRepository;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $characteristicValueRepository;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    protected $characteristicRange = array('height', 'bust', 'waist', 'hips');

    /**
     * @param EntityRepository $productRepository
     * @param EntityRepository $characteristicRepository
     * @param EntityRepository $characteristicValueRepository
     * @param EntityManager $em
     */
    public function __construct(
        EntityRepository $productRepository,
        EntityRepository $characteristicRepository,
        EntityRepository $characteristicValueRepository,
        EntityManager $em
    )
    {
        $this->productRepository = $productRepository;
        $this->characteristicRepository = $characteristicRepository;
        $this->characteristicValueRepository = $characteristicValueRepository;
        $this->em = $em;
    }

    /**
     * @return array
     */
    public function getProductForDashboard()
    {
        return $this->productRepository->findAll();
    }

    /**
     * @param array $characteristics
     * @return array
     */
    public function getProductByCharacteristics(array $characteristics)
    {

        if (count($characteristics) == 0)
            return false;

        $qb = $this->em->createQueryBuilder();

        $characteristicsChoice = array();
        $characteristicsQuery = array();

        foreach ($characteristics as $characteristic) {

            if ($characteristic['characteristic']->getType() == Characteristic::selectType) {
                $characteristicsChoice[$characteristic['characteristic']->getId()][] = $characteristic;
            } else {
                $characteristicsQuery[] = $characteristic;
            }
        }

        $key = 0;

        //for remember where need AND or OR
        $characteristicStack = array();

        if (count($characteristicsQuery) > 0) {

            foreach ($characteristicsQuery as $characteristic) {

                $compare = isset($characteristic['compare']) ? $characteristic['compare'] : '=';

                if ($key == 0) {
                    $qb->select('t' . $key);
                    $qb->from('Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue', 't' . $key)
                        ->andWhere("t$key.characteristic = " . $characteristic['characteristic']->getId());

                } else {
                    $qb->leftJoin('Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue',
                        't' . $key,
                        Expr\Join::WITH,
                        't' . ($key - 1) . '.entity = t' . $key . '.entity AND t' . $key . '.characteristic =' . $characteristic['characteristic']->getId(),
                        $indexBy = null);
                }

                //main query
                if (is_array($characteristic['value'])) {
                    $query = '(t' . $key . '.' . $characteristic['characteristic']->getType() . ' >= :value'. $key.$characteristic['value']['from'];
                    $query .= ' AND t' . $key . '.' . $characteristic['characteristic']->getType() . ' <= :value' . $key.$characteristic['value']['to'].')';
                } else {
                    $query = 't' . $key . '.' . $characteristic['characteristic']->getType() .$compare. ':value' . $key;
                }

                if (!in_array($characteristic['characteristic']->getId(), $characteristicStack)) {

                    if (!isset($querys[$characteristic['characteristic']->getId()])) {
                        $querys[$characteristic['characteristic']->getId()] = $query;
                    }

                    //for remember where need AND or OR
                    $characteristicStack[] = $characteristic['characteristic']->getId();
                } else {
                    $querys[$characteristic['characteristic']->getId()] .= ' OR ' . $query;
                }

                if (is_array($characteristic['value'])) {
                    $qb->setParameter('value' . $key.$characteristic['value']['from'], $characteristic['value']['from']);
                    $qb->setParameter('value' . $key.$characteristic['value']['to'], $characteristic['value']['to']);
                }else{
                    $qb->setParameter('value' . $key, $characteristic['value']);
                }


                $key++;
            }

            foreach ($querys as $query) {
                $qb->andWhere($query);
            }


        }

        if (count($characteristicsChoice) > 0) {

            foreach ($characteristicsChoice as $characteristicArray) {

//                if ((count($characteristicsQuery) == 0) && ($key == 0)) {
//                    $qb->select('t' . $key);
//                    $qb->from('Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue', 't' . $key);
//                    $qb->andWhere("(t$key.characteristic = " . $characteristicArray[0]['characteristic']->getId().")");
//                } else {
//                    $qb->leftJoin('Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue',
//                        't' . $key,
//                        Expr\Join::WITH,
//                        't' . ($key - 1) . '.entity = t' . $key . '.entity AND t'. $key . '.characteristic =' . $characteristicArray[0]['characteristic']->getId(),
//                        $indexBy = null);
//                }

                $queryStack = null;

                foreach ($characteristicArray as $characteristicChoice) {

                    if ((count($characteristicsQuery) == 0) && ($key == 0)) {
                        $qb->select('t' . $key);
                        $qb->from('Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue', 't' . $key);
                        $qb->andWhere("(t$key.characteristic = " . $characteristicChoice['characteristic']->getId().")");
                    } else {
                        $qb->leftJoin('Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue',
                            't' . $key,
                            Expr\Join::WITH,
                            't' . ($key - 1) . '.entity = t' . $key . '.entity AND t'. $key . '.characteristic =' . $characteristicChoice['characteristic']->getId(),
                            $indexBy = null);
                    }

                    //main query
                    $query = 't' . $key . '.' . $characteristicChoice['characteristic']->getType() . ' = :value' . $key;

                    if (!isset($queryStack)) {
                        $queryStack = $query;
                    }else{
                        $queryStack .= ' OR '.$query;
                    }
                    $qb->setParameter('value' . $key, $characteristicChoice['value']);
                    $key++;
                }

                $qb->andWhere($queryStack);
            }

        }

        foreach ($characteristics as $key => $characteristic) {
//            if ($key == 0) {
//
//                $qb->select('t' . $key);
//                $qb->from('Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue', 't' . $key)
//                    ->andWhere("t$key.characteristic = " . $characteristic['characteristic']->getId())
//                    ->andWhere("t$key." . $characteristic['characteristic']->getType() . " = " . $characteristic['value']);
//
//            } else {
//                $qb->leftJoin('Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue',
//                    't' . $key,
//                    Expr\Join::WITH,
//                    't' . ($key - 1) . '.entity = t' . $key . '.entity',
//                    $indexBy = null)
//                    ->andWhere("(t$key.characteristic = " . $characteristic['characteristic']->getId() . " AND
//                    (t$key." . $characteristic['characteristic']->getType() . "=" . $characteristic['value'] . "
//                    OR t$key." . $characteristic['characteristic']->getType() . "=" . 10 .")
//                    )");
//            }
        }
        $qb->groupBy("t0.entity");

//        print_r($qb->getQuery()->getSQL());

        $characteristicValues = $qb->getQuery()->getResult();

        $products = array();

        foreach ($characteristicValues as $characteristicValue) {
            $products[] = $characteristicValue->getEntity();
        }

        return $products;

    }

    /**
     * @param $alias
     * @return null|object
     */
    public function getProductByAlias($alias)
    {
        return $this->productRepository->findOneBy(array(
            'alias' => (string)$alias,
            'publish' => true,
        ));
    }

    /**
     * @param array $data
     * @return array
     */
    public function search(array $data)
    {

        $characteristics = array();

        //search by type (teens, artists, ...)
        if ($data['type'] != 'type') {
            $characteristic = $this->characteristicRepository->findOneBy(array(
                'systemName' => (string)$data['type'],
            ));

            if ($characteristic) {
                $characteristics[] = array(
                    'characteristic' => $characteristic,
                    'value' => true
                );
            }
        }

        unset($data['type']);

        foreach ($data as $key => $param) {

            $parseCharacteristic = explode('_', $key);
            $characteristicId = $parseCharacteristic[0];

            $characteristic = $this->characteristicRepository->find($characteristicId);

            //check is characteristic gange
//            TODO: refactor to dynamic form
            if(in_array($characteristic->getSystemName(), $this->characteristicRange)){

                $characteristicValue = array('from'=>$parseCharacteristic[1], 'to'=>$parseCharacteristic[2]);

            }else{

                $characteristicValue = $parseCharacteristic[1];

            }

            $characteristicCompare = isset($parseCharacteristic[2]) ? $parseCharacteristic[2] : '=';

            $characteristics[] = array(
                'characteristic' => $characteristic,
                'value' => $characteristicValue,
                'compare' => $characteristicCompare,
            );
        }

        return $this->getProductByCharacteristics($characteristics);
    }
}