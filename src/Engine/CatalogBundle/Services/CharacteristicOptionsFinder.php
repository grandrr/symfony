<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\CatalogBundle\Services;

use Doctrine\ORM\EntityRepository;
use Engine\CatalogBundle\Entity\Characteristic\Characteristic;

/**
 * Class CategoryFinder
 * @package Acme\BlogBundle\Services
 */
class CharacteristicOptionsFinder
{
    /**
     * @var \Engine\CatalogBundle\Entity\Characteristic\CharacteristicRepository
     */
    protected $characteristicRepository;

    /**
     * @var \Engine\CatalogBundle\Entity\Characteristic\CharacteristicRepository
     */
    protected $characteristicOptionsRepository;

    /**
     * @var \Router
     */
    protected $router;

    /**
     * @param EntityRepository $characteristicRepository
     * @param EntityRepository $characteristicOptionsRepository
     * @param $router
     */
    public function __construct(
        EntityRepository $characteristicRepository,
        EntityRepository $characteristicOptionsRepository,
        $router
    )
    {
        $this->characteristicRepository = $characteristicRepository;
        $this->characteristicOptionsRepository = $characteristicOptionsRepository;
        $this->router = $router;

    }

//    public function getCharacteristicsForDashboard()
//    {
//        return $this->characteristicOptionsRepository->findBy(array( //'publish' => true,
//        ), array(
//            'position' => 'ASC',
//        ));
//    }

    /**
     * @param $id
     * @return array|null
     */
    public function getCharacteristicOptionsJsonForDashboard($id)
    {
        $result = array();

        $entitys = $this->characteristicOptionsRepository->findBy(
            array(
                'characteristic' => $this->characteristicRepository->find($id),
            ),
            array(
                'position' => 'ASC',
            )
        );

        if ($entitys) {
            foreach ($entitys as $entity) {
                $result = $this->getEntitys($entity, $result);
            }
        }
        return $result;
    }

    /**
     * @param $title
     * @return bool|null|object
     */
    public function getOptionByTitle($title)
    {
        $option = false;

        $option = $this->characteristicOptionsRepository->findOneBy(array('title'=>$title));

        return $option;
    }

    /**
     * @param $entity
     * @param null $result
     * @return array|null
     */
    protected function getEntitys($entity, $result = null)
    {
        $data['id'] = $entity->getId();
        $data['name'] = $entity->getTitle().'('.$entity->getViewName().')';
        $data['publish'] = $entity->getPublish();
        $data['editLink'] = $this->router->generate('engine_characteristic_options_dashboard_edit', array('id' => $entity->getId()));
        $data['removeLink'] = $this->router->generate('engine_characteristic_options_dashboard_remove', array('id' => $entity->getId()));
        $data['open'] = true;

        $result[] = $data;

        return $result;
    }
}