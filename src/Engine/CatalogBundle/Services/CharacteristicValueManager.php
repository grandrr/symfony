<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\CatalogBundle\Services;

use Engine\EngineBundle\Services\AbstractEntityManager;
use Engine\CatalogBundle\Entity\Characteristic\CharacteristicValueFactory;
use Engine\CatalogBundle\Entity\Characteristic\Characteristic;
use Doctrine\ORM\EntityRepository;
use Engine\FileBundle\Entity\File;

/**
 * Class ProductManager
 * @package Acme\BlogBundle\Services
 */
class CharacteristicValueManager extends AbstractEntityManager
{
    /**
     * @var CharacteristicValueFinder
     */
    protected $characteristicValueFinder;

    /**
     * @var CharacteristicValueFactory
     */
    protected $characteristicValueFactory;

    /**
     * @var CharacteristicOptionRepository
     */
    protected $characteristicOptionRepository;

    /**
     * @var CharacteristicRepository
     */
    protected $characteristicRepository;

    /**
     * @param EntityRepository $entityRepository
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param \Engine\EngineBundle\Entity\EntityFactoryInterface $entityFactory
     * @param CharacteristicValueFinder $characteristicValueFinder
     * @param CharacteristicValueFactory $characteristicValueFactory
     * @param EntityRepository $characteristicOptionRepository
     */
    public function __construct(
        $entityRepository,
        $entityManager,
        $entityFactory,
        CharacteristicValueFinder $characteristicValueFinder,
        CharacteristicValueFactory $characteristicValueFactory,
        EntityRepository $characteristicOptionRepository,
        EntityRepository $characteristicRepository
    )
    {
        parent::__construct($entityRepository, $entityManager, $entityFactory);

        $this->characteristicValueFinder = $characteristicValueFinder;
        $this->characteristicValueFactory = $characteristicValueFactory;
        $this->characteristicOptionRepository = $characteristicOptionRepository;
        $this->characteristicRepository = $characteristicRepository;
    }

    /**
     * @param array $data
     * @return mixed|void
     */
    public function createFromArrayData(array $data)
    {
        foreach ($data['characteristics'] as $characteristicName => $characteristicValue) {

            $characteristicData = explode('_', $characteristicName);

            //disable create characteristic value from hidden field
            if (!isset($characteristicData[2])) {
                $data['characteristicId'] = $characteristicData[0];
                $data['characteristicType'] = $characteristicData[1];
                $data['characteristicValue'] = $characteristicValue;

                $this->createFromValidData($data);
            }
        }
    }

    /**
     * @param array $data
     * @return null|object
     */
    public function updateFromValidDataArray(array $data)
    {
        foreach ($data['characteristics'] as $characteristicName => $characteristicValue) {

            $characteristicData = explode('_', $characteristicName);

            if ($characteristicValueEntity = $this->characteristicValueFinder->
                getCharacteristicValueForProductFormDashboard($data['id'], $characteristicData[0])
            ) {
                $getValueFunction = 'set' . $characteristicData[1];

                if ($characteristicData[1] == Characteristic::selectType) {
                    $characteristicValue = $this->characteristicOptionRepository->find($characteristicValue);
                }

                //disable set values for file
                if ($characteristicData[1] != Characteristic::fileType) {
                    $characteristicValueEntity->$getValueFunction($characteristicValue);
                    $this->saveEntity($characteristicValueEntity);
                }

            } else {

                $data['characteristicId'] = $characteristicData[0];
                $data['characteristicType'] = $characteristicData[1];
                $data['characteristicValue'] = $characteristicValue;
                $data['productId'] = $data['id'];

                $this->createFromValidData($data);
            }

        }
    }

    /**
     * @param array $data
     * @return bool|null|object|void
     */
    protected function updateData(array $data)
    {
        $characteristic = $this->characteristicRepository->find($data['characteristicId']);

        if ($characteristicValueEntity = $this->characteristicValueFinder->
            getCharacteristicValueForProductFormDashboard($data['productId'], $data['characteristicId'])
        ) {

            if ($characteristic->getType() == Characteristic::selectType) {
                $data['characteristicValue'] = $this->characteristicOptionRepository->find($data['characteristicValue']);
            }

            $characteristicValueEntity->setValue($data['characteristicValue']);

            return $this->saveEntity($characteristicValueEntity);

        } else {

            $data['characteristicType'] = $characteristic->getType();

            return $this->createFromValidData($data);
        }
    }

    /**
     * @param array $data
     */
    public function addMultifile(array $data)
    {
        $characteristicValueEntity = $this->entityRepository->find($data['id']);
//        $characteristicValueEntity->addMultifile($data['file']);
//        $this->saveEntity($characteristicValueEntity);
        $data['file']->setCharacteristicValue($characteristicValueEntity);
        $this->saveEntity($data['file']);
    }
} 