<?php

namespace Engine\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{
    public function indexAction()
    {
        $characteristicFinder = $this->get('engine.catalog.services.characteristic_finder');

        $characteristicsType = array(
            'man',
            'woman',
            'talents',
            'artists',
        );

        $productFinder = $this->get('engine.catalog.services.product_finder');

        $result = array();

        foreach ($characteristicsType as $type) {
            $characteristic = $characteristicFinder->getCharacteristicBySystemName($type);
            $result[$type] = $productFinder->getProductByCharacteristics(array(
                    array('characteristic' => $characteristic, 'value' => true, 'field' => 'checkbox', 'compare' => '=')
                )
            );

        }

        return $this->render('EngineCatalogBundle:Index:index.html.twig', array(
            'categorys' => $result
        ));
    }

    public function categoryAction(Request $request)
    {
        $characteristicFinder = $this->get('engine.catalog.services.characteristic_finder');

        $characteristic = $characteristicFinder->getCharacteristicBySystemName($request->get('category'));
//        $characteristic1 = $characteristicFinder->getCharacteristicBySystemName('description');
//        $characteristic2 = $characteristicFinder->getCharacteristicBySystemName('artists');

        $characteristics[] = array('characteristic' => $characteristic, 'value' => true);
//        $characteristics[] = array('characteristic' => $characteristic1, 'value' => 'ss');
//        $characteristics[] = array('characteristic' => $characteristic2, 'value' => true);

        $productFinder = $this->get('engine.catalog.services.product_finder');

        if ($request->isMethod('post')) {

            $requestArray = $request->request->all();
            $requestArray['type'] = $request->get('category');
            $products = $productFinder->search($requestArray);

        }else{

            $products = $productFinder->getProductByCharacteristics($characteristics);

        }

        return $this->render('EngineCatalogBundle:Index:category.html.twig',
            array(
                'category' => $characteristic,
                'option' => '',
                'products' => $products,
                'request' => array_keys($request->request->all()),
                'categoryAlias' => $request->get('category')
            )
        );
    }

    public function genderAction(Request $request)
    {
        $characteristicFinder = $this->get('engine.catalog.services.characteristic_finder');

        $characteristic = $characteristicFinder->getCharacteristicBySystemName('gender');

        $optionFinder = $this->get('engine.catalog.services.characteristicOptions_finder');

        $optionGender = $optionFinder->getOptionByTitle($request->get('gender'));

        $characteristics[] = array('characteristic' => $characteristic, 'value' => $optionGender->getId());

        $productFinder = $this->get('engine.catalog.services.product_finder');

        if ($request->isMethod('post')) {

            $requestArray = $request->request->all();
            $requestArray['gender'] = $request->get('gender');
            $products = $productFinder->search($requestArray);

        }else{

            $products = $productFinder->getProductByCharacteristics($characteristics);

        }

        return $this->render('EngineCatalogBundle:Index:category.html.twig',
            array(
                'category' => $characteristic,
                'option' => $optionGender,
                'products' => $products,
                'request' => array_keys($request->request->all()),
                'categoryAlias' => $optionGender->getTitle()
            )
        );
    }

    public function showByCharacteristicAction(Request $request)
    {
        $productFinder = $this->get('engine.catalog.services.product_finder');

        $products = $productFinder->search($request->request->all());

        return $this->render('EngineCatalogBundle:Index:showByCharacteristic.html.twig',
            array(
                'products' => $products,
                'request' => $request->request->all()
            )
        );
    }

    public function showByAliasAction(Request $request)
    {
        $productFinder = $this->get('engine.catalog.services.product_finder');

        $product = $productFinder->getProductByAlias($request->get('alias'));

        return $this->render('EngineCatalogBundle:Index:showByAlias.html.twig', array('product' => $product));
    }
}
