<?php

namespace Engine\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Engine\CatalogBundle\Entity\Characteristic\Characteristic;
use Symfony\Component\HttpFoundation\JsonResponse;

use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Column\BlankColumn;
use APY\DataGridBundle\Grid\Column\TextColumn;
use APY\DataGridBundle\Grid\Column\ActionsColumn;
use APY\DataGridBundle\Grid\Action\MassAction;
use APY\DataGridBundle\Grid\Action\DeleteMassAction;
use APY\DataGridBundle\Grid\Action\RowAction;

class ProductDashboardController extends Controller
{
    public function indexAction()
    {
//        $productFinder = $this->get('engine.catalog.services.product_finder');
//
//        return $this->render('EngineCatalogBundle:ProductDashboard:index.html.twig',
//            array('products' => $productFinder->getProductForDashboard())
//        );

        $source = new Entity('Engine\CatalogBundle\Entity\Product\Product');

        // Get a grid instance
        $grid = $this->get('grid');

        // Attach the source to the grid
        $grid->setSource($source);

        // Configuration of the grid
        // Set the selector of the number of items per page
        $grid->setLimits(array(20, 30, 50));

        // Set the default page
        $grid->setDefaultPage(1);

        $productRepository = $this->get('engine.catalog.entity.product_repository');

        $categoryColumn = new BlankColumn(array('id' => 'category', 'title'=>'category', 'size'=>120));
        $grid->addColumn($categoryColumn);

        $typeColumn = new BlankColumn(array('id' => 'type', 'title'=>'type', 'size'=>20));
        $grid->addColumn($typeColumn);

        $source->manipulateRow(
            function ($row) use ($productRepository) {

                $product = $productRepository->findOneBy(array('id' => $row->getField('id')));

                $value = array();

                $charMan = $product->getCharacteristicValueBySystemName('man');
                $charWoman = $product->getCharacteristicValueBySystemName('woman');
                $charTalent = $product->getCharacteristicValueBySystemName('talents');
                $charArt = $product->getCharacteristicValueBySystemName('artists');

                $value[] = $charMan && $charMan->getValue() ? 'man' : '';
                $value[] = $charWoman && $charWoman->getValue() ? 'wom.' : '';
                $value[] = $charTalent && $charTalent->getValue() ? 'tal.' : '';
                $value[] = $charArt && $charArt->getValue() ? 'art.' : '';

                $value = implode(' ', $value);
                $row->setField('category', $value);

                if ($type = $product->getCharacteristicValueBySystemName('type')) {
                    $row->setField('type', $type->getValue()->getViewName());
                }

                return $row;
            }
        );

        // Specify route parameters for the edit action
        $rowEditAction = new RowAction('Edit', 'engine_catalog_dashboard_edit');
        $rowEditAction->setRouteParameters(array('id'));
        $grid->addRowAction($rowEditAction);

        $rowDeleteAction = new RowAction('Delete', 'engine_catalog_dashboard_delete');
        $rowDeleteAction->setRouteParameters(array('id'));
        $grid->addRowAction($rowDeleteAction);

        $grid->setActionsColumnSize(10);

        // Manage the grid redirection, exports and the response of the controller
        return $grid->getGridResponse('EngineCatalogBundle:ProductDashboard:index_grid.html.twig');

    }

    public function createAction(Request $request)
    {
        $formType = $this->get('engine.catalog.forms.product.create');
        $form = $this->createForm($formType);

        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $productManager = $this->get('engine.catalog.services.product_manager');
                $product = $productManager->createFromValidData($form->getData());
                $characteristicValueManager = $this->get('engine.catalog.services.characteristicValue_manager');
                $characteristicValueManager->createFromArrayData(
                    array_merge($form->getData(), array('productId' => $product->getId()))
                );
                return $this->redirect($this->generateUrl('engine_catalog_dashboard'));
            }
        }

        return $this->render('EngineCatalogBundle:ProductDashboard:edit.html.twig', array(
            'form' => $form->createView(),
            'product' => false,
        ));
    }

    public function editAction(Request $request)
    {
//        $fileRepository = $this->get('engine.file.entity.fileRepository');
//        $file = $fileRepository->find('76');
//
//        $characteristicValueRepository = $this->get('engine.catalog.entity.characteristicValues_repository');
//        $characteristicValue = $characteristicValueRepository->find('51');
//
//        $characteristicValueManager = $this->get('engine.catalog.services.characteristicValue_manager');
//        $characteristicValueManager->addMultifile(array(
//            'id' => $characteristicValue->getId(),
//            'file' => $file
//        ));


        $formEditType = $this->get('engine.catalog.forms.product.edit');
        $form = $this->createForm($formEditType);

        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $productManager = $this->get('engine.catalog.services.product_manager');
                $productManager->updateFromValidData($form->getData());

                $characteristicValueManager = $this->get('engine.catalog.services.characteristicValue_manager');
                $characteristicValueManager->updateFromValidDataArray($form->getData());

                return $this->redirect($this->generateUrl('engine_catalog_dashboard'));
            }
        } else {
            $repository = $this->get('engine.catalog.entity.product_repository');
            $entity = $repository->find($request->get('id'));
            $characteristics = array();

            //dynamic set value characteristic to form
            foreach ($entity->getCharacteristicValue() as $characteristicValue) {
                $key = $characteristicValue->getCharacteristic()->getId() . '_' . $characteristicValue->getType();
                $getValueFunction = 'get' . $characteristicValue->getType();

                if ($characteristicValue->getType() == Characteristic::selectType) {
                    $characteristics[$key] = $characteristicValue->$getValueFunction()->getId();
                } elseif ($characteristicValue->getType() == Characteristic::fileType) {
                    $key = $key . '_hidden';
                    if ($characteristicValue->$getValueFunction()) {
                        $characteristics[$key] = $characteristicValue->$getValueFunction()->getId();
                    }
                } elseif ($characteristicValue->getType() == Characteristic::multiFileType) {
                    $key = $key . '_hidden';
                    $characteristics[$key] = $characteristicValue->getId();
//                    foreach ($characteristicValue->getMultifile() as $file) {
//                        print_r($file->getId());
//                    }
                } else {
                    $characteristics[$key] = $characteristicValue->$getValueFunction();
                }
            }

            $form->setData(array(
                'id' => $entity->getId(),
                'alias' => $entity->getAlias(),
                'publish' => $entity->getPublish(),
                'characteristics' => $characteristics,
            ));
        }

        return $this->render('EngineCatalogBundle:ProductDashboard:edit.html.twig', array(
            'form' => $form->createView(),
            'product' => $entity,
        ));
    }

    public function uploadFileAction(Request $request)
    {
        $fileManager = $this->get('engine.file.services.fileManager');

        $file = $fileManager->uploadFile($request);

        $characteristicValueManager = $this->get('engine.catalog.services.characteristicValue_manager');

        $characteristicValueManager->updateFromValidData(array(
            'productId' => $request->get('productId'),
            'characteristicId' => $request->get('characteristicId'),
            'characteristicValue' => $file
        ));

        $response = new JsonResponse();
        $response->setData(array(
            'product' => $request->get('productId'),
            'characteristic' => $request->get('characteristicId'),
            'file' => $file->getId()
        ));
        return $response;
    }

    public function multiuploadFileAction(Request $request)
    {
        $fileManager = $this->get('engine.file.services.fileManager');

        $file = $fileManager->uploadFile($request);

        $characteristicValueManager = $this->get('engine.catalog.services.characteristicValue_manager');

        $characteristicValue = $characteristicValueManager->updateFromValidData(array(
            'productId' => $request->get('productId'),
            'characteristicId' => $request->get('characteristicId'),
            'characteristicValue' => null,
        ));

        $fileManager->addCharacteristicValue(array(
            'file' => $file,
            'characteristicValue' => $characteristicValue,
        ));

        $response = new JsonResponse();
        $response->setData(array(
            'product' => $request->get('productId'),
            'characteristic' => $request->get('characteristicId'),
            'characteristicValue' => $characteristicValue->getId(),
            'file' => $file->getId()
        ));
        return $response;
    }

    public function deleteAction(Request $request)
    {
        $fileManager = $this->get('engine.catalog.services.product_manager');
        $fileManager->remove($request->get('id'));

        return $this->redirect($this->generateUrl('engine_catalog_dashboard'));
    }
}