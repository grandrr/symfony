<?php

namespace Engine\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CharacteristicDashboardController extends Controller
{
    public function publishCharacteristicAjaxAction(Request $request)
    {
        $characteristicsManager = $this->get('engine.catalog.services.characteristic_manager');

        $result = $characteristicsManager->changePublish($request->request->get('id'));

        $response = new JsonResponse();
        $response->setData(array('result' => $result));
        return $response;
    }

    public function getCharacteristicsAjaxAction()
    {
        $characteristicsFinder = $this->get('engine.catalog.services.characteristic_finder');
        $data = $characteristicsFinder->getCharacteristicsJsonForDashboard();

        $response = new JsonResponse();
        $response->setData($data);
        return $response;
    }

    public function updateCharacteristicsAjaxAction(Request $request)
    {
        $characteristicsManager = $this->get('engine.catalog.services.characteristic_manager');
        $characteristicsManager->updatePosition($request->request->get('data'));

        $response = new JsonResponse();
        $response->setData(array('result' => $characteristicsManager->updatePosition($request->request->get('data'))));
        return $response;
    }

    public function indexAction()
    {
        return $this->render('EngineCatalogBundle:CharacteristicDashboard:index.html.twig');
    }

    public function createAction(Request $request)
    {
        $formType = $this->get('engine.catalog.forms.characteristic.create');
        $form = $this->createForm($formType);

        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $characteristicManager = $this->get('engine.catalog.services.characteristic_manager');
                $characteristicManager->createFromValidData($form->getData());
                return $this->redirect($this->generateUrl('engine_characteristic_dashboard'));
            }
        }

        return $this->render('EngineCatalogBundle:CharacteristicDashboard:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request)
    {
        $formEditType = $this->get('engine.catalog.forms.characteristic.edit');
        $form = $this->createForm($formEditType);

        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $characteristicManager = $this->get('engine.catalog.services.characteristic_manager');
                $characteristicManager->updateFromValidData($form->getData());
                return $this->redirect($this->generateUrl('engine_characteristic_dashboard'));
            }
        } else {
            $repository = $this->get('engine.catalog.entity.characteristic_repository');
            $entity = $repository->find($request->get('id'));
            $form->setData(array(
                'id' => $entity->getId(),
                'type' => $entity->getType(),
                'systemName' => $entity->getSystemName(),
                'required' => $entity->getRequired(),
                'system' => $entity->getSystem(),
                'publish' => $entity->getPublish(),
            ));
        }

        return $this->render('EngineCatalogBundle:CharacteristicDashboard:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function removeAction(Request $request)
    {
        $characteristicManager = $this->get('engine.catalog.services.characteristic_manager');
        $characteristicManager->remove($request->get('id'));

        return $this->redirect($this->generateUrl('engine_characteristic_dashboard'));
    }
}
