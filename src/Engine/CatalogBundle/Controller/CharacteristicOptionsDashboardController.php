<?php

namespace Engine\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CharacteristicOptionsDashboardController extends Controller
{
    public function getCharacteristicOptionsAjaxAction(Request $request)
    {
        $characteristicsFinder = $this->get('engine.catalog.services.characteristicOptions_finder');
        $data = $characteristicsFinder->getCharacteristicOptionsJsonForDashboard($request->get('id'));

        $response = new JsonResponse();
        $response->setData($data);
        return $response;
    }

    public function publishCharacteristicOptionsAjaxAction(Request $request)
    {
        $characteristicOptionsManager = $this->get('engine.catalog.services.characteristicOptions_manager');

        $result = $characteristicOptionsManager->changePublish($request->request->get('id'));

        $response = new JsonResponse();
        $response->setData(array('result' => $result));
        return $response;
    }

    public function updateCharacteristicOptionsAjaxAction(Request $request)
    {
        $characteristicsManager = $this->get('engine.catalog.services.characteristicOptions_manager');
        $characteristicsManager->updatePosition($request->request->get('data'));

        $response = new JsonResponse();
        $response->setData(array('result' => $characteristicsManager->updatePosition($request->request->get('data'))));
        return $response;
    }

    public function indexAction(Request $request)
    {
        $repository = $this->get('engine.catalog.entity.characteristic_repository');
        return $this->render('EngineCatalogBundle:CharacteristicOptionsDashboard:index.html.twig',array(
            'characteristic'=>$repository->find($request->get('id'))
        ));
    }

    public function createAction(Request $request)
    {
        $formType = $this->get('engine.catalog.forms.characteristicOptions.create');
        $form = $this->createForm($formType);

        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $data['characteristicId'] = $request->get('id');
                $characteristicManager = $this->get('engine.catalog.services.characteristicOptions_manager');
                $characteristicManager->createFromValidData($data);
                return $this->redirect($this->generateUrl('engine_characteristic_dashboard'));
            }
        }

        return $this->render('EngineCatalogBundle:CharacteristicOptionsDashboard:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request)
    {
        $formEditType = $this->get('engine.catalog.forms.characteristicOptions.edit');
        $form = $this->createForm($formEditType);

        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $characteristicOptionsManager = $this->get('engine.catalog.services.characteristicOptions_manager');
                $characteristicOptionsManager->updateFromValidData($form->getData());
                return $this->redirect($this->generateUrl('engine_characteristic_dashboard'));
            }
        } else {
            $repository = $this->get('engine.catalog.entity.characteristicOption_repository');
            $entity = $repository->find($request->get('id'));
            $form->setData(array(
                'id' => $entity->getId(),
                'title' => $entity->getTitle(),
                'viewName' => $entity->getViewName(),
                'publish' => $entity->getPublish(),
            ));
        }

        return $this->render('EngineCatalogBundle:CharacteristicOptionsDashboard:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function removeAction(Request $request)
    {
        $characteristicManager = $this->get('engine.catalog.services.characteristicOptions_manager');
        $characteristicManager->remove($request->get('id'));

        return $this->redirect($this->generateUrl('engine_characteristic_dashboard'));
    }
}
