<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/6/14
 * Time: 1:49 PM
 */

namespace Engine\PageBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class Create extends AbstractType
{

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            //disable html5 validation
            'attr' => array('novalidate' => 'novalidate'),
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention' => 'page_item',
        ));
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'label' => 'Title',
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3)),
                ),
            ))
            ->add('description', 'textarea', array(
                'label' => 'Description',
                'attr' => array('class'=>'tiny'),
                'constraints' => array(
                    new NotBlank(),
                ),
            ))
            ->add('alias', 'text', array(
                'label' => 'Alias',
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3)),
                ),
            ))
            ->add('date', 'text', array(
                'label' => 'Date',
            ))
            ->add('publish', 'checkbox', array(
                'label' => 'Publish',
            ))
            ->add('save', 'submit');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'page';
    }
}