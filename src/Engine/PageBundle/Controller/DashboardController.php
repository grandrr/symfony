<?php

namespace Engine\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class DashboardController extends Controller
{
    public function publishPagesAjaxAction(Request $request)
    {
        $pagesManager = $this->get('engine.page.services.page_manager');

        $result = $pagesManager->changePagePublish($request->request->get('id'));

        $response = new JsonResponse();
        $response->setData(array('result' => $result));
        return $response;
    }

    public function getPagesAjaxAction()
    {
        $pagesFinder = $this->get('engine.page.services.page_finder');
        $pages = $pagesFinder->getPagesJsonForDashboard();

        $response = new JsonResponse();
        $response->setData($pages);
        return $response;
    }

    public function updatePagesAjaxAction(Request $request)
    {
        $pagesManager = $this->get('engine.page.services.page_manager');
        $pagesManager->updatePagesTree($request->request->get('data'));

        $response = new JsonResponse();
        $response->setData(array('result' => $pagesManager->updatePagesTree($request->request->get('data'))));
        return $response;
    }

    public function indexPageAction()
    {
        $pagesFinder = $this->get('engine.page.services.page_finder');
        $pages = $pagesFinder->getPagesForDashboard();

        return $this->render('EnginePageBundle:Dashboard:indexPage.html.twig', array(
            'pages' => $pages,
        ));
    }

    public function createPageAction(Request $request)
    {
        $formType = $this->get('engine.page.forms.create');
        $form = $this->createForm($formType);

        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $postManager = $this->get('engine.page.services.page_manager');
                $postManager->createFromValidData($form->getData());
                return $this->redirect($this->generateUrl('engine_page_index'));
            }
        }

        return $this->render('EnginePageBundle:Dashboard:editPage.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editPageAction(Request $request)
    {
        $formPostEditType = $this->get('engine.page.forms.edit');
        $form = $this->createForm($formPostEditType);

        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $postManager = $this->get('engine.page.services.page_manager');
                $postManager->updateFromValidData($form->getData());
                return $this->redirect($this->generateUrl('engine_page_index'));
            }
        } else {
            $repository = $this->get('engine.page.entity.page_repository');
            $entity = $repository->find($request->get('id'));
            $form->setData(array(
                'id' => $entity->getId(),
                'alias' => $entity->getAlias(),
                'title' => $entity->getTitle(),
                'description' => $entity->getDescription(),
                'date' => $entity->getDate()->format('Y-m-d H:i'),
                'publish' => $entity->getPublish(),
            ));
        }

        return $this->render('EnginePageBundle:Dashboard:editPage.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function removePageAction(Request $request)
    {
        $pageManager = $this->get('engine.page.services.page_manager');
        $pageManager->remove($request->get('id'));

        return $this->redirect($this->generateUrl('engine_page_index'));
    }
}
