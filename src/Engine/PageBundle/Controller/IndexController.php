<?php

namespace Engine\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    public function showPageAction($alias)
    {
        $pagesFinder = $this->get('engine.page.services.page_finder');
        return $this->render('EnginePageBundle:Index:showPage.html.twig', array(
            'page' => $pagesFinder->getPublishPageByAlias($alias),
        ));
    }
}
