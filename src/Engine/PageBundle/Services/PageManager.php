<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\PageBundle\Services;

use Engine\EngineBundle\Services\AbstractEntityManager;

/**
 * Class CategoryService
 * @package Acme\BlogBundle\Services
 */
class PageManager extends AbstractEntityManager
{
    /**
     * @param array $data
     * @return null|object
     */
    public function updateFromValidData(array $data)
    {
        if ($data['date'])
            $data['date'] = new \DateTime($data['date']);

        if ($entity = $this->updateData($data)) {
            return $this->saveEntity($entity);
        }
        return false;
    }

    public function updatePagesTree(array $data)
    {
        $position = 0;
        foreach ($data as $page) {
            $entity = $this->entityRepository->find($page['id']);
            $parentPage = $this->entityRepository->find($page['pid']);
            $entity->setParent($parentPage);
            $entity->setPosition($position);
            $this->saveEntity($entity);
            $position++;
        }
    }

    public function changePagePublish($id)
    {
        $entity = $this->entityRepository->find($id);
        if ($entity->getPublish()) {
            $entity->setPublish(false);
        } else {
            $entity->setPublish(true);
        }
        $this->saveEntity($entity);
        return true;
    }
} 