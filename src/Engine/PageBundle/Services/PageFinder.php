<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\PageBundle\Services;

use Doctrine\ORM\EntityRepository;

/**
 * Class CategoryFinder
 * @package Acme\BlogBundle\Services
 */
class PageFinder
{
    /**
     * @var \Engine\PageBundle\Entity\PageRepository
     */
    protected $pageRepository;

    /**
     * @var \Router
     */
    protected $router;

    /**
     * @param EntityRepository $pageRepository
     * @param $router
     */
    public function __construct(
        EntityRepository $pageRepository,
        $router
    )
    {
        $this->pageRepository = $pageRepository;
        $this->router = $router;
    }

    /**
     * @return array|null
     */
    public function getPagesJsonForDashboard()
    {
        $result = array();

        $pages = $this->pageRepository->findBy(array(
            'parent' => null,
        ), array(
            'position' => 'ASC',
        ));

        if ($pages) {
            foreach ($pages as $key => $page) {
                $result = $this->getPages($page, $result);
            }
        }
        return $result;
    }

    /**
     * @param $page
     * @param null $result
     * @return array|null
     */
    protected function getPages($page, $result = null)
    {
        $data['id'] = $page->getId();
        $data['pId'] = $page->getParent() ? $page->getParent()->getId() : null;
        $data['name'] = $page->getTitle();
        $data['publish'] = $page->getPublish();
        $data['editLink'] = $this->router->generate('engine_page_edit', array('id' => $page->getId()));
        $data['removeLink'] = $this->router->generate('engine_page_remove', array('id' => $page->getId()));
        $data['open'] = true;

        $result[] = $data;

        if ($page->getChildren()) {
            foreach ($page->getChildren() as $childPage) {
                $result = $this->getPages($childPage, $result);
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getPagesForMain()
    {
        return $this->pageRepository->findBy(array(
            'publish' => true,
            'parent' => null,
        ), array(
            'position' => 'ASC',
        ));
    }

    /**
     * @return array
     */
    public function getPagesForDashboard()
    {
        return $this->pageRepository->findAll();
    }

    /**
     * @param $id
     * @return null|object
     */
    public function getPageById($id)
    {
        return $this->pageRepository->find($id);
    }

    /**
     * @param $alias
     * @return null|object
     */
    public function getPageByAlias($alias)
    {
        return $this->pageRepository->findOneBy(array(
            'alias' => $alias,
        ));
    }

    /**
     * @param $alias
     * @return null|object
     */
    public function getPublishPageByAlias($alias)
    {
        return $this->pageRepository->findOneBy(array(
            'alias' => $alias,
            'publish' => true
        ));
    }
} 