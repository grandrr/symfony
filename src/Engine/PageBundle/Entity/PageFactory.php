<?php

namespace Engine\PageBundle\Entity;

use Engine\EngineBundle\Entity\EntityFactoryInterface;

class PageFactory implements EntityFactoryInterface
{
    public function create(array $data = null)
    {
        $post = new Page(
            $data['alias'],
            $data['title'],
            $data['description'],
            $data['publish']
        );

        return $post;
    }
}
