<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/1/14
 * Time: 3:58 PM
 */

namespace Engine\PageBundle\Twig\Extension;

use Engine\PageBundle\Services\PageFinder;
use Symfony\Bundle\FrameworkBundle\Routing\Router;


class PageBundleExtension extends \Twig_Extension
{

    /**
     * @var \Engine\PageBundle\Services\PageFinder
     */
    protected $pageFinder;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    protected $router;

    /**
     * @param PageFinder $pageFinder
     * @param Router $router
     */
    public function __construct(PageFinder $pageFinder, Router $router)
    {
        $this->pageFinder = $pageFinder;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            'getPage' => new \Twig_Function_Method($this, 'getPage'),
        );
    }

    public function getPage($alias)
    {
        return $this->pageFinder->getPageByAlias($alias);
    }

    public function getName()
    {
        return 'page_extension';
    }

} 