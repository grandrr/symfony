<?php

namespace Engine\PlanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Plan
 *
 * @ORM\Table("plans")
 * @ORM\Entity(repositoryClass="Engine\PlanBundle\Entity\PlanRepository")
 */
class Plan
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var text
     *
     * @ORM\Column(name="plan", type="string", length=255)
     */
    private $plan;

    /**
     * @var text
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var text
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @param $plan
     * @param $username
     * @param $email
     */
    function __construct($plan, $username, $email)
    {
        $this->setPlan($plan);
        $this->setUsername($username);
        $this->setEmail($email);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $plan
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;
    }

    /**
     * @return mixed
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param \Engine\PlanBundle\Entity\text $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return \Engine\PlanBundle\Entity\text
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param \Engine\PlanBundle\Entity\text $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return \Engine\PlanBundle\Entity\text
     */
    public function getEmail()
    {
        return $this->email;
    }
}