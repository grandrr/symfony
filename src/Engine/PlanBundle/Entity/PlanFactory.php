<?php

namespace Engine\PlanBundle\Entity;

use Engine\EngineBundle\Entity\EntityFactoryInterface;

class PlanFactory implements EntityFactoryInterface
{
    public function create(array $data = null)
    {
        return new Plan(
            $data['plan'],
            $data['username'],
            $data['email']
        );
    }
}
