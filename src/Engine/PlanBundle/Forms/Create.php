<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/6/14
 * Time: 1:49 PM
 */

namespace Engine\PlanBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class Create extends AbstractType
{
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            //disable html5 validation
            'attr' => array('novalidate' => 'novalidate'),
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention' => 'page_item',
        ));
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plan', 'text', array(
                'required' => true,
            ))
            ->add('username', 'text', array(
                'data' => 'Username',
                'required' => true,
                'constraints' => array(
                    new NotBlank(),
                )
            ))
            ->add('email', 'text', array(
                'data' => 'Email',
                'required' => true,
                'constraints' => array(
                    new NotBlank(),
                    new Email()
                )
            ))
            ->add('password', 'text', array(
                'data' => 'Password',
                'required' => true,
                'constraints' => array(
                    new NotBlank(),
                )
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'plan';
    }
}