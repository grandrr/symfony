<?php

namespace Engine\PlanBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    public function indexAction()
    {
        $planRepository = $this->get('engine.plan.entity.planRepository');

        $plans = $planRepository->findAll();

        return $this->render('EnginePlanBundle:Dashboard:index.html.twig', array(
            'plans' => $plans,
        ));
    }
}
