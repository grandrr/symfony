<?php

namespace Engine\PlanBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class PlanController extends Controller
{
    public function planAction()
    {
        $pageFinder = $this->get('engine.page.services.page_finder');
        $planPageId = $this->container->getParameter('planPageId');

        $page = $pageFinder->getPageById($planPageId);

        $formType = $this->get('engine.plan.forms.create');
        $form = $this->createForm($formType);

        return $this->render('EnginePlanBundle:Plan:plan.html.twig', array(
            'page' => $page,
            'form' => $form->createView(),
        ));
    }

    public function ajaxCreateAction(Request $request)
    {
        $formType = $this->get('engine.plan.forms.create');
        $form = $this->createForm($formType);

        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $planManager = $this->get('engine.plan.services.planManager');
                $planManager->createFromValidData($form->getData());
                $success = true;
                $result = $this->renderView('EnginePlanBundle:Plan:ajaxSuccess.html.twig');
            } else {
                $success = false;
                $result = $this->renderView('EnginePlanBundle:Plan:ajaxCreate.html.twig', array(
                    'form' => $form->createView(),
                ));
            }

            $response = new JsonResponse();
            $response->setData(array(
                'result' => $result,
                'success' => $success,
            ));
        }

        return $response;
    }
}
