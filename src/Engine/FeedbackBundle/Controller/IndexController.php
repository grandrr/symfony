<?php

namespace Engine\FeedbackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class IndexController extends Controller
{
    public function feedbackAction()
    {
        $formType = $this->get('engine_feedback.formCreate');
        $form = $this->createForm($formType);

        return $this->render('EngineFeedbackBundle:Index:feedback.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function createAction(Request $request)
    {
        $formType = $this->get('engine_feedback.formCreate');
        $form = $this->createForm($formType);

        if ($request->isMethod('post')) {

            $form->submit($request);

            if ($form->isValid()) {

                $mailerService = $this->get('engine_feedback.mailerService');
                $mailerService->send($form->getData()['name'], $form->getData()['phone'], $form->getData()['message']);

                $html = $this->renderView('EngineFeedbackBundle:Index:success.html.twig', array(
                    'form' => $form->createView(),
                ));
            }else{
                $html = $this->renderView('EngineFeedbackBundle:Index:feedback.html.twig', array(
                    'form' => $form->createView(),
                ));
            }
        }

        $response = new JsonResponse();
        $response->setData(array('success'=>true, 'html'=>$html));
        return $response;
    }
}
