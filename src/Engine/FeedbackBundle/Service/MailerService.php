<?php

namespace Engine\FeedbackBundle\Service;

class MailerService
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var
     */
    protected $to;

    /**
     * @param \Swift_Mailer $mailer
     * @param $to
     */
    function __construct(\Swift_Mailer $mailer, $to)
    {
        $this->mailer = $mailer;
        $this->to = $to;
    }

    /**
     * @param $name
     * @param $phone
     * @param $message
     */
    public function send($name, $phone, $message)
    {
        $message = "Feedback message from: $name\r\nphone: $phone\r\nmessage: $message";

        mail($this->to, 'Fpeople Feedback', $message);

//        $swiftMessage = \Swift_Message::newInstance()
//            ->setSubject('Fpeople Feedback')
////            ->setFrom('send@example.com')
//            ->setTo($this->to)
//            ->setBody($message)
//        ;
//
//        $this->mailer->send($swiftMessage);
    }
}
