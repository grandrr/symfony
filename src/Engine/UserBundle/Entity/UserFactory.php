<?php

namespace Engine\UserBundle\Entity;

use Engine\EngineBundle\Entity\EntityFactoryInterface;
use Doctrine\ORM\EntityRepository;

class UserFactory implements EntityFactoryInterface
{
    /**
     * @var UserRepository
     */
    protected $roleRepository;

    /**
     * @param EntityRepository $roleRepository
     */
    function __construct(EntityRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param array $data
     * @return User
     */
    public function create(array $data = null)
    {
        $user = new User(
            $data['username'],
            $data['email'],
            $data['password'],
            $data['active']
        );

        $role = $this->roleRepository->find(User::ADMIN_ROLE);

        $user->addRole($role);

        return $user;

    }
}
