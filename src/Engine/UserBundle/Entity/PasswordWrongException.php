<?php

namespace Engine\UserBundle\Entity;

use Symfony\Component\Security\Core\Exception\AuthenticationException;


/**
 * PasswordWrongException is thrown if a Password not match.
 *
 */
class PasswordWrongException extends AuthenticationException
{
    private $username;

    /**
     * {@inheritDoc}
     */
    public function getMessageKey()
    {
        return 'Wrong password.';
    }

    /**
     * Get the username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the username.
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * {@inheritDoc}
     */
    public function serialize()
    {
        return serialize(array(
            $this->username,
            parent::serialize(),
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($str)
    {
        list($this->username, $parentData) = unserialize($str);

        parent::unserialize($parentData);
    }
}
