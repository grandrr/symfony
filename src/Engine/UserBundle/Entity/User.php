<?php

namespace Engine\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * Engine\UserBundle\Entity\User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Engine\UserBundle\Entity\UserRepository")
 * @GRID\Source(columns="id, username, email, isActive")
 */

class User implements UserInterface, \Serializable
{

    CONST ADMIN_ROLE = 1;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     * @GRID\Column(size="5", filterable=false)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     * @GRID\Column(title="Username", size="120", type="text", filterable=false)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     * @GRID\Column(title="Email", size="120", type="text", filterable=false)
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     * @GRID\Column(title="Is active", type="boolean", filterable=false)
     */
    private $isActive;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     *
     */
    private $roles;

    /**
     * @param $username
     * @param $email
     * @param $password
     * @param $active
     */
    public function __construct($username, $email, $password, $active)
    {
        $this->setUsername($username);
        $this->setEmail($email);
        $this->setPassword($password);
        $this->setActive($active);

        $this->roles = new ArrayCollection();
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid(null, true));
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $isActive
     */
    public function setActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }


    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password = crypt($password, $password);
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
//        return $this->roles->toArray();
//        return array('ROLE_ADMIN'=>'ROLE_ADMIN');
        $roles = array();
        if ($this->roles) {
            foreach ($this->roles as $role) {
                $roles[] = $role->getRole();
            }
        }
        return $roles;
    }

    /**
     * @param $role
     */
    public function addRole($role)
    {
        $this->roles[] = $role;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }
}