<?php

namespace Engine\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Column\TextColumn;
use APY\DataGridBundle\Grid\Column\ActionsColumn;
use APY\DataGridBundle\Grid\Action\MassAction;
use APY\DataGridBundle\Grid\Action\DeleteMassAction;
use APY\DataGridBundle\Grid\Action\RowAction;

class DashboardController extends Controller
{
    public function indexAction()
    {
//        $userFinder = $this->get('engine.user.services.userFinder');
//        $users = $userFinder->getUsersForDashboard();
//
//        return $this->render('EngineUserBundle:Dashboard:index.html.twig', array('users' => $users));

        $source = new Entity('Engine\UserBundle\Entity\User');

        // Get a grid instance
        $grid = $this->get('grid');

        // Attach the source to the grid
        $grid->setSource($source);

        // Configuration of the grid
        // Set the selector of the number of items per page
        $grid->setLimits(array(5, 10, 15));

        // Set the default page
        $grid->setDefaultPage(1);

        // Specify route parameters for the edit action
        $rowEditAction = new RowAction('Edit', 'engine_user_dashboard_editUser');
        $rowEditAction->setRouteParameters(array('id'));
        $grid->addRowAction($rowEditAction);
        $grid->setActionsColumnSize(10);

        // Manage the grid redirection, exports and the response of the controller
        return $grid->getGridResponse('EngineUserBundle:Dashboard:index_grid.html.twig');
    }

    public function deleteUserAction(Request $request)
    {
        return $this->redirect($this->generateUrl('engine_user_dashboard_index'));
    }

    public function createUserAction(Request $request)
    {
        $formType = $this->get('engine.user.forms.create');
        $form = $this->createForm($formType);

        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $userManager = $this->get('engine.user.services.user_manager');
                $userManager->createFromValidData($form->getData());
                return $this->redirect($this->generateUrl('engine_user_dashboard_index'));
            }
        }

        return $this->render('EngineUserBundle:Dashboard:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editUserAction(Request $request)
    {
        $formEditType = $this->get('engine.user.forms.edit');
        $form = $this->createForm($formEditType);

        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $userManager = $this->get('engine.user.services.user_manager');
                $userManager->updateFromValidData($form->getData());
                return $this->redirect($this->generateUrl('engine_user_dashboard_index'));
            }
        } else {
            $repository = $this->get('engine.user.entity.user_repository');
            $entity = $repository->find($request->get('id'));
            $form->setData(array(
                'id' => $entity->getId(),
                'username' => $entity->getUsername(),
                'email' => $entity->getEmail(),
                'active' => $entity->getActive(),
            ));
        }

        return $this->render('EngineUserBundle:Dashboard:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
