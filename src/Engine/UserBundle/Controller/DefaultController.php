<?php

namespace Engine\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Engine\UserBundle\Entity\PasswordWrongException;

class DefaultController extends Controller
{
    public function loginAction()
    {
        $sessionManager = $this->get('engine.user.services.session_manager');

        $username = false;
        if ($user = $sessionManager->getUser()) {
            $username = $user->getUsername();
        }

        return $this->render(
            'EngineUserBundle:Default:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $username,
                'message' => false
            )
        );
    }

    public function checkAction(Request $request)
    {
        $message = false;
        try {

            $userManager = $this->get('engine.user.services.user_manager');
            $user = $userManager->checkFromRequest($request);
            $sessionManager = $this->get('engine.user.services.session_manager');
            $sessionManager->setUser($user);

        } catch (UsernameNotFoundException $e) {
            $message = $e->getMessage();
            return $this->render(
                'EngineUserBundle:Default:login.html.twig',
                array(
                    // last username entered by the user
                    'last_username' => false,
                    'message' => $message
                )
            );
        } catch (PasswordWrongException $e) {
            $message = $e->getMessage();
            return $this->render(
                'EngineUserBundle:Default:login.html.twig',
                array(
                    // last username entered by the user
                    'last_username' => false,
                    'message' => $message
                )
            );
        }
        return $this->redirect($this->generateUrl('engine_engine_homepage'));
    }

    public function logoutAction()
    {
        $sessionManager = $this->get('engine.user.services.session_manager');
        $sessionManager->unsetUser();
        return $this->redirect($this->generateUrl('login'));
    }
}
