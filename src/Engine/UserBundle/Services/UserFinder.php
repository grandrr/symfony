<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\UserBundle\Services;

use Doctrine\ORM\EntityManager;
use Engine\UserBundle\Entity\UserRepository;

/**
 * Class CategoryService
 * @package Engine\BlogBundle\Services
 */
class UserFinder
{
    /**
     * @var \Engine\UserBundle\Entity\UserRepository
     */
    protected $entityRepository;

    /**
     * @param UserRepository $entityRepository
     */
    public function __construct(
        UserRepository $entityRepository
    )
    {
        $this->entityRepository = $entityRepository;
    }

    public function getUsersForDashboard()
    {
        return $this->entityRepository->findAll();
    }
} 