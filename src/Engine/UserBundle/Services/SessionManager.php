<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\UserBundle\Services;

use Engine\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class SessionManager
 * @package Engine\UserBundle\Services
 */
class SessionManager
{
    /**
     * @var session
     */
    protected $session;

    /**
     * @var firewall
     */
    protected $firewall;

    /**
     * @param $session
     * @param $firewall
     */
    public function __construct($session, $firewall)
    {
        $this->session = $session;
        $this->firewall = $firewall;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $token = new UsernamePasswordToken($user, null, $this->firewall, $user->getRoles());

        $this->session->set('_security_' . $this->firewall, serialize($token));
        $this->session->save();
    }

    /**
     *
     */
    public function unsetUser()
    {
        $this->session->set('_security_' . $this->firewall, null);
        $this->session->save();
    }

    /**
     * @return bool
     */
    public function getUser()
    {
        $session = $this->session->get('_security_' . $this->firewall);
        if ($session) {
            $token = \unserialize($session);
            return $token->getUser();
        }
        return false;
    }
} 