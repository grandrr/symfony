<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\UserBundle\Services;

use Symfony\Component\Security\Core\Util\StringUtils;
use Engine\UserBundle\Entity\PasswordWrongException;
use Engine\EngineBundle\Services\AbstractEntityManager;

/**
 * Class CategoryService
 * @package Engine\BlogBundle\Services
 */
class UserManager extends AbstractEntityManager
{
    /**
     * @param array $data
     * @return bool|null|object
     */
    protected function updateData(array $data)
    {
        if ($entity = $this->entityRepository->find($data['id'])) {
            unset($data['id']);
            if ($data['password'] == '')
                unset($data['password']);
            foreach ($data as $field => $value) {
                $funtion = "set$field";
                if (\method_exists($entity, $funtion))
                    $entity->$funtion($value);
            }
            return $entity;
        }
        return false;
    }

    /**
     * @param $request
     * @return mixed
     * @throws \Engine\UserBundle\Entity\PasswordWrongException
     */
    public function checkFromRequest($request)
    {
        $user = $this->entityRepository->loadUserByUsername($request->request->get('_username'));
        if ($user) {
            $bool = StringUtils::equals(
                crypt($request->request->get('_password'), $user->getPassword()),
                $user->getPassword()
            );
            if (!$bool) {
                $message = 'Wrong password.';
                throw new PasswordWrongException($message);
            }
        }
        return $user;
    }
}