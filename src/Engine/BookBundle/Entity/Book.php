<?php

namespace Engine\BookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Engine\CatalogBundle\Entity\Product\Product;

/**
 * Book
 *
 * @ORM\Table("books")
 * @ORM\Entity(repositoryClass="Engine\BookBundle\Entity\BookRepository")
 */
class Book
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Engine\CatalogBundle\Entity\Product\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     **/
    private $product;

    /**
     * @var text
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var text
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;


    function __construct(Product $product, $name, $email)
    {
        $this->setProduct($product);
        $this->setName($name);
        $this->setEmail($email);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return \Engine\PlanBundle\Entity\text
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \Engine\PlanBundle\Entity\text
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }
}