<?php

namespace Engine\BookBundle\Entity;

use Engine\EngineBundle\Entity\EntityFactoryInterface;
use Engine\CatalogBundle\Entity\Product\ProductRepository;

class BookFactory implements EntityFactoryInterface
{

    protected $productRepository;

    function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function create(array $data = null)
    {

        $product = $this->productRepository->find($data['product']);

        $book = new Book(
            $product,
            $data['name'],
            $data['email']
        );

        return $book;

    }
}
