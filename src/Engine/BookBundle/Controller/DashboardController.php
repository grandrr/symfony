<?php

namespace Engine\BookBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    public function indexAction()
    {
        $bookRepository = $this->get('engine.book.entity.bookRepository');

        $books = $bookRepository->findAll();

        return $this->render('EngineBookBundle:Dashboard:index.html.twig', array(
            'books' => $books,
        ));
    }
}
