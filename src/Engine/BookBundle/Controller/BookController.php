<?php

namespace Engine\BookBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class BookController extends Controller
{
    public function bookAction()
    {
        $formType = $this->get('engine.book.forms.create');
        $form = $this->createForm($formType);

        return $this->render('EngineBookBundle:Book:book.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function ajaxCreateAction(Request $request)
    {
        $formType = $this->get('engine.book.forms.create');
        $form = $this->createForm($formType);

        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $bookManager = $this->get('engine.book.services.bookManager');
                $bookManager->createFromValidData($form->getData());
                $success = true;
                $result = $this->renderView('EngineBookBundle:Book:ajaxSuccess.html.twig');
            } else {
                $success = true;
                $result = $this->renderView('EngineBookBundle:Book:book.html.twig', array(
                    'form' => $form->createView(),
                ));
            }

            $response = new JsonResponse();
            $response->setData(array(
                'result' => $result,
                'success' => $success,
            ));
        }

        return $response;
    }
}
