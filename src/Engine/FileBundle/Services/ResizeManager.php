<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\FileBundle\Services;

use Engine\FileBundle\Entity\File;


/**
 * Class ProductManager
 * @package Acme\BlogBundle\Services
 */
class ResizeManager
{

    protected $resizeRules;

    protected $storagePath;

    protected $staticPath;

    function __construct(
        $resizeRules,
        $storagePath,
        $staticPath
    )
    {
        $this->resizeRules = $resizeRules;
        $this->storagePath = $storagePath;
        $this->staticPath = $staticPath;
    }

    public function resize($path)
    {
        $path = explode('/', $path);
        $options = $this->resizeRules[$path[1]];
        $staticFolder = dirname(__FILE__) . '/../../../..' . $this->staticPath . File::generatePath($path[2]);
        $staticPath = $staticFolder . $path[1] . '_' . $path[2];
        $storagePath = dirname(__FILE__) . '/../../../..' . $this->storagePath . File::generatePath($path[2]) . (int)$path[2];

        $ext = explode('.', $path[2])[1];

        if (!file_exists($staticPath)) {
            if (!file_exists($staticFolder))
                mkdir($staticFolder, 0755, true);

            $image = \WideImage_WideImage::load($storagePath);

            $resized = $image->resize($options['width'], $options['height']);

            $calculatedPosX = $options['width'] - $resized->getWidth() > 0 ?
                ($options['width'] - $resized->getWidth()) / 2 : 0;

            $calculatedPosY = $options['height'] - $resized->getHeight() > 0 ?
                ($options['height'] - $resized->getHeight()) / 2 : 0;

            $white = $image->allocateColor(255, 255, 255);

            $resized = $resized->resizeCanvas(
                $options['width'],
                $options['height'],
                $calculatedPosX,
                $calculatedPosY,
                $white
            );
            $resized->saveToFile($staticPath, $options['quality']);
        }

        $image = new \WideImage_WideImage();

        $image = $image->load($staticPath);
        $image->output($ext);
    }
}