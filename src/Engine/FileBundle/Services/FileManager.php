<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Engine\FileBundle\Services;

use Engine\EngineBundle\Services\AbstractEntityManager;
use Symfony\Component\HttpFoundation\Request;
use Engine\FileBundle\Entity\File;

/**
 * Class ProductManager
 * @package Acme\BlogBundle\Services
 */
class FileManager extends AbstractEntityManager
{

    const jpeg_mime_type = 'image/jpeg;';
    const png_mime_type = 'image/png;';

    public static $mimeTypes = array(
        self::jpeg_mime_type => 'jpg',
        self::png_mime_type => 'png'
    );

    /**
     * @var
     */
    protected $storagePath;

    /**
     * @var
     */
    protected $staticFolder;

    /**
     * @param \Doctrine\ORM\EntityRepository $entityRepository
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param \Engine\EngineBundle\Entity\EntityFactoryInterface $entityFactory
     * @param $storagePath
     * @param $staticFolder
     */
    function __construct($entityRepository, $entityManager, $entityFactory, $storagePath, $staticFolder)
    {
        $this->entityRepository = $entityRepository;
        $this->entityManager = $entityManager;
        $this->entityFactory = $entityFactory;
        $this->storagePath = $storagePath;
        $this->staticFolder = $staticFolder;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function uploadFile(Request $request)
    {
        if ($tempFile = $request->files->get('fileToUpload')) {

//            self::$mimeTypes[$tempFile->getMimeType()]

            $file = $this->entityFactory->create();

            $this->saveEntity($file);

            $tempFile->move(
                dirname(__FILE__) . '/../../../../' . $this->storagePath . File::generateFolder($file->getId()),
                File::generateName($file->getId())
            );

            return $file;
        }
    }

    /**
     * @param array $data
     */
    public function addCharacteristicValue(array $data)
    {
        $data['file']->setCharacteristicValue($data['characteristicValue']);
        $this->saveEntity($data['file']);
    }

    /**
     * @param $id
     * @param $rule
     * @return string
     */
    public function buildUrl($id, $rule)
    {
        $folder = explode('/', $this->staticFolder);
        $fileName = File::generateName($id);
        $fileFolder = File::generateFolder($id);
        $storagePath = dirname(__FILE__) . '/../../../..' . $this->storagePath . '/' . $fileFolder . '/' . $fileName;

        if (file_exists($storagePath)) {
            return '/' . $folder[2] . '/' . $rule . '/' . $fileName . '.' . $this->getExtensionById($id);
        } else {
            return 'no_file';
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function getExtensionById($id)
    {
        $fileName = File::generateName($id);
        $fileFolder = File::generateFolder($id);
        $storagePath = dirname(__FILE__) . '/../../../..' . $this->storagePath . '/' . $fileFolder . '/' . $fileName;
        if (file_exists($storagePath)) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $storagePath);
            finfo_close($finfo);
            $mimetype = explode(' ', $mimetype);
            if($ext = self::$mimeTypes[$mimetype[0]]){
                return $ext;
            }
            return 'unknown_extension';
        } else {
            return 'no_file';
        }
    }
}