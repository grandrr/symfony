<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/1/14
 * Time: 3:58 PM
 */

namespace Engine\FileBundle\Twig\Extension;

use Engine\FileBundle\Services\FileManager;
use Doctrine\ORM\EntityRepository;


class FileBundleExtension extends \Twig_Extension
{

    /**
     * @var \Engine\FileBundle\Services\FileManager
     */
    protected $fileManager;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $characteristicValuesRepository;

    /**
     * @param FileManager $fileManager
     * @param EntityRepository $characteristicValuesRepository
     */
    public function __construct(FileManager $fileManager, EntityRepository $characteristicValuesRepository)
    {
        $this->fileManager = $fileManager;
        $this->characteristicValuesRepository = $characteristicValuesRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            'fileSrc' => new \Twig_Function_Method($this, 'fileSrc'),
            'imageSrc' => new \Twig_Function_Method($this, 'imageSrc'),
            'multifile' => new \Twig_Function_Method($this, 'multifile')
        );
    }

    public function fileSrc($idFile)
    {
        return 'test' . $idFile;
    }

    public function imageSrc($data)
    {
        return $this->fileManager->buildUrl($data['id'], $data['rule']);
    }

    public function multifile($data)
    {
        $id = array();

        if ($characteristicValue = $this->characteristicValuesRepository->find($data['characteristicValue'])) {

            foreach ($characteristicValue->getMultifile() as $file) {
                $id[] = $file->getId();
            }

        }

        return $id;
    }

    public function getName()
    {
        return 'file_extension';
    }

} 