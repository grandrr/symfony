<?php

namespace Engine\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * File
 *
 * @ORM\Table("files")
 * @ORM\Entity(repositoryClass="Engine\FileBundle\Entity\FileRepository")
 */
class File
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue")
     * @ORM\JoinColumn(name="characteristic_value_id", referencedColumnName="id")
     */
    private $characteristicValue;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue $characteristicValue
     */
    public function setCharacteristicValue(\Engine\CatalogBundle\Entity\Characteristic\CharacteristicValue $characteristicValue)
    {
        $this->characteristicValue = $characteristicValue;
    }

    /**
     * Generate uri
     *
     * @param int $id
     * @return string
     */
    public static function generateFolder($id)
    {
        $number = self::generateName($id);
        return self::generatePath($number);
    }

    /**
     * @param $number
     * @return string
     */
    public static function generatePath($number)
    {
        return '/' . \substr($number, 0, 3) . '/' . \substr($number, 3, 3) . '/';
    }

    /**
     * Generate uri
     *
     * @param int $id
     * @return string
     */
    public static function generateName($id)
    {
        return \strrev(\sprintf('%09d', $id));
    }
}
