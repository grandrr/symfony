<?php

namespace Engine\FileBundle\Entity;

use Engine\EngineBundle\Entity\EntityFactoryInterface;

class FileFactory implements EntityFactoryInterface
{
    public function create(array $data = null)
    {
        return new File();
    }
}
