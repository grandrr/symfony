<?php

namespace Engine\FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class FileDashboardController extends Controller
{
    public function ajaxRemoveAction(Request $request)
    {
        if($request->isXmlHttpRequest()){

            $fileManager = $this->get('engine.file.services.fileManager');
            $fileManager->remove($request->get('id'));

            $response = new JsonResponse();
            $response->setData(array('result' => true));
            return $response;
        }
    }
}
