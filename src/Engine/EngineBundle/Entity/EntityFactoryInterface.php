<?php

namespace Engine\EngineBundle\Entity;

interface EntityFactoryInterface
{
    public function create(array $data = null);

}
