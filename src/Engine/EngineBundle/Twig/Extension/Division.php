<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/1/14
 * Time: 3:58 PM
 */

namespace Engine\EngineBundle\Twig\Extension;


class Division extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('division', array($this, 'division')),
        );
    }

    public function division($key, $division)
    {
        return ($key+1)%$division;

    }

    public function getName()
    {
        return 'division_extension';
    }

} 