<?php

namespace Engine\EngineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    public function indexAction()
    {
        return $this->render('EngineEngineBundle:Dashboard:index.html.twig');
    }
}
