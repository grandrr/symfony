<?php
/**
 * {@license}
 */

namespace Engine\PageNavigationBundle\Service;

use Engine\ModelBundle\Entity\RepositoryInterface;
use Engine\NavigationBundle\Service\BuilderInterface;
use Engine\SearchFilterBundle\SearchFilter\Factory as SearchFilterFactory;
use Knp\Menu\FactoryInterface as MenuFactoryInterface;

/**
 * @author  naydav <valeriy.nayda@gmail.com>
 */
class Builder implements BuilderInterface
{
    /**
     * @var \Engine\ModelBundle\Entity\RepositoryInterface
     */
    protected $pagesRepository;

    /**
     * @var \Knp\Menu\FactoryInterface
     */
    protected $navigationFactory;

    /**
     * @var \Engine\SearchFilterBundle\SearchFilter\Factory
     */
    protected $searchFilterFactory;

    /**
     * @var array
     */
    protected $defaultOptions = [];

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @param \Engine\ModelBundle\Entity\RepositoryInterface $pagesRepository
     * @param \Knp\Menu\FactoryInterface $navigationFactory
     * @param \Engine\SearchFilterBundle\SearchFilter\Factory $searchFilterFactory
     * @param [] $defaultOptions
     */
    public function __construct(
        RepositoryInterface $pagesRepository,
        MenuFactoryInterface $navigationFactory,
        SearchFilterFactory $searchFilterFactory,
        array $defaultOptions = []
    ) {
        $this->pagesRepository = $pagesRepository;
        $this->navigationFactory = $navigationFactory;
        $this->searchFilterFactory = $searchFilterFactory;
        $this->defaultOptions = $defaultOptions;
    }

    /**
     * {@inheritdoc}
     */
    public function build(array $options = [])
    {
        $this->options = \array_merge($this->defaultOptions, $options);

        return $this->buildRecursively($this->getRootPage(), true);
    }

    /**
     * @param \Engine\PageBundle\Entity\ReadPageInterface $page
     * @param bool $isRoot
     * @return \Knp\Menu\ItemInterface
     */
    protected function buildRecursively($page, $isRoot = false)
    {
        $menu = $isRoot ? $this->createRootMenuItem($page) : $this->createMenuItem($page);

        foreach ($this->getChildren($page) as $childPage) {
            $menu->addChild($this->buildRecursively($childPage));
        }
        return $menu;
    }

    /**
     * @param \Engine\PageBundle\Entity\ReadPageInterface $page
     * @return \Knp\Menu\ItemInterface
     */
    protected function createRootMenuItem($page)
    {
        return $this->navigationFactory->createItem($page->getId(), [
            'label' => $this->options['rootMenu']['label'],
            'route' => $this->options['rootMenu']['route'],
        ]);
    }

    /**
     * @param \Engine\PageBundle\Entity\ReadPageInterface $page
     * @return \Knp\Menu\ItemInterface
     */
    protected function createMenuItem($page)
    {
        if (isset($this->options['modules'][$page->getId()])) {
            // Modules
            $urlDefinition = $this->options['modules'][$page->getId()];
        } else {
            // Defaults pages
            $urlDefinition = $page->isHasExternalUrl() ? [
                'uri' => $page->getUrl(),
                'linkAttributes' => ['target' => '_blank'],
            ] : [
                'route' => '_engine_page_show_by_url',
                'routeParameters' => ['url' => $page->getUrl()],
            ];
        }

        $menuDefinition = \array_merge([
            'label' => $page->getTitle(),
        ], $urlDefinition);

        return $this->navigationFactory->createItem($page->getId(), $menuDefinition);
    }

    /**
     * @return \Engine\PageBundle\Entity\ReadPageInterface
     * @throws \InvalidArgumentException
     */
    protected function getRootPage()
    {
        if (!isset($this->options['ROOT_PAGE_ID'])) {
            throw new \InvalidArgumentException('Root page id has not been set.');
        }
        return $this->pagesRepository->restore($this->options['ROOT_PAGE_ID']);
    }

    /**
     * @param \Engine\PageBundle\Entity\ReadPageInterface $page
     * @return \Engine\PageBundle\Entity\ReadPageInterface[]
     */
    protected function getChildren($page)
    {
        return $this->pagesRepository->findByFilter($this->searchFilterFactory->createFilterChain([
            'i18n.withTranslations',
            'nested.children' => [$page, true],
            'page.inMenu',
        ]));
    }
}
