<?php
/**
 * {@license}
 */

namespace Engine\PageNavigationBundle\Twig\Extension;

use Engine\NavigationBundle\Service\Renderer\Menu;
use Engine\NavigationBundle\Service\Renderer\Breadcrumbs;

/**
 * @link http://symfony.com/doc/current/cookbook/templating/twig_extension.html
 * @link http://twig.sensiolabs.org/doc/advanced.html#creating-an-extension
 *
 * @author  naydav <valeriy.nayda@gmail.com>
 */
class PageNavigationExtension extends \Twig_Extension
{
    /**
     * @var \Engine\NavigationBundle\Service\Renderer\Menu
     */
    protected $topMenu;

    /**
     * @var \Engine\NavigationBundle\Service\Renderer\Menu
     */
    protected $bottomMenu;

    /**
     * @var \Engine\NavigationBundle\Service\Renderer\Breadcrumbs
     */
    protected $breadcrumbs;

    /**
     * @param \Engine\NavigationBundle\Service\Renderer\Menu $topMenu
     * @param \Engine\NavigationBundle\Service\Renderer\Menu $bottomMenu
     * @param \Engine\NavigationBundle\Service\Renderer\Breadcrumbs $breadcrumbs
     */
    public function __construct(
        Menu $topMenu,
        Menu $bottomMenu,
        Breadcrumbs $breadcrumbs
    ) {
        $this->topMenu = $topMenu;
        $this->bottomMenu = $bottomMenu;
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'page_top_menu',
                [$this->topMenu, 'render'],
                ['is_safe' => ['html']]
            ),
            new \Twig_SimpleFunction(
                'page_bottom_menu',
                [$this->bottomMenu, 'render'],
                ['is_safe' => ['html']]
            ),
            new \Twig_SimpleFunction(
                'page_breadcrumbs',
                [$this->breadcrumbs, 'render'],
                ['is_safe' => ['html']]
            ),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'engine_page_navigation';
    }
}
