<?php
/**
 * {@license}
 */

namespace Engine\PageNavigationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @author  naydav <valeriy.nayda@gmail.com>
 */
class EnginePageNavigationBundle extends Bundle
{
}
