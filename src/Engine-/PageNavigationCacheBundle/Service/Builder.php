<?php
/**
 * {@license}
 */

namespace Engine\PageNavigationCacheBundle\Service;

use Engine\CacheBundle\Model\AbstractDecorator;
use Engine\NavigationBundle\Service\BuilderInterface;

/**
 * @author  naydav <valeriy.nayda@gmail.com>
 */
class Builder extends AbstractDecorator implements BuilderInterface
{
    /**
     * @var \Engine\NavigationBundle\Service\BuilderInterface
     */
    protected $decoratedObject;

    /**
     * {@inheritdoc}
     */
    protected function checkDecoratedObject($decoratedObject)
    {
        if (!$decoratedObject instanceof BuilderInterface) {
            throw new \InvalidArgumentException('Wrong decorated object.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function build(array $options = [])
    {
        $cacheKey = $this->generateCacheKey(['page', 'navigation', 'builder', $options]);

        return $this->callDecoratedMethodThroughCache('build', $cacheKey, [$options]);
    }
}
