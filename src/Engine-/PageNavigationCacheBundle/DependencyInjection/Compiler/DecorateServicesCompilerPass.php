<?php
/**
 * {@license}
 */

namespace Engine\PageNavigationCacheBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * To learn more see {@link http://symfony.com/doc/current/cookbook/service_container/compiler_passes.html}
 *
 * @author naydav <valeriy.nayda@gmail.com>
 */
class DecorateServicesCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        // with this solution we can decorate builder only once
        $container->setAlias('engine.pageNavigation.builder', 'engine.pageNavigationCache.builder.cache');
    }
}
