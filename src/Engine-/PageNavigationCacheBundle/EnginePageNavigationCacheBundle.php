<?php
/**
 * {@license}
 */

namespace Engine\PageNavigationCacheBundle;

use Engine\PageNavigationCacheBundle\DependencyInjection\Compiler\DecorateServicesCompilerPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @author  naydav <valeriy.nayda@gmail.com>
 */
class EnginePageNavigationCacheBundle extends Bundle
{
    /**
     * {@inheritDoc}
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new DecorateServicesCompilerPass());
    }
}
