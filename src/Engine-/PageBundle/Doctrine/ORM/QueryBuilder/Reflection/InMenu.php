<?php
/**
 * {@license}
 */

namespace Engine\PageBundle\Doctrine\ORM\QueryBuilder\Reflection;

use Engine\DoctrineBundle\ORM\QueryBuilder\Reflection\AbstractReflection;

/**
 * {@inheritdoc}
 */
class InMenu extends AbstractReflection
{
    /**
     * {@inheritdoc}
     */
    protected function doReflect()
    {
        $this->queryBuilder->andWhere('t.inMenu = 1');
    }
}
