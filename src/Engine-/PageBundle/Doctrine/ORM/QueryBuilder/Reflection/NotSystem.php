<?php
/**
 * {@license}
 */

namespace Engine\PageBundle\Doctrine\ORM\QueryBuilder\Reflection;

use Engine\DoctrineBundle\ORM\QueryBuilder\Reflection\AbstractReflection;

/**
 * {@inheritdoc}
 */
class NotSystem extends AbstractReflection
{
    /**
     * {@inheritdoc}
     */
    protected function doReflect()
    {
        $this->queryBuilder->andWhere('t.isSystem != 1');
    }
}
