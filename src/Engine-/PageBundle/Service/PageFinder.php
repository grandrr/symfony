<?php
/**
 * {@license}
 */

namespace Engine\PageBundle\Service;

use Engine\DoctrineBundle\Service\AbstractFinder;
use Engine\ModelBundle\Entity\RepositoryInterface;
use Engine\SearchFilterBundle\SearchFilter\Factory as SearchFilterFactory;

/**
 * {@inheritdoc}
 */
class PageFinder extends AbstractFinder implements PageFinderInterface
{
    /**
     * @var \Engine\SearchFilterBundle\SearchFilter\Factory
     */
    protected $searchFilterFactory;

    /**
     * @var \Engine\ModelBundle\Entity\RepositoryInterface
     */
    protected $pageRepository;

    /**
     * @param \Engine\SearchFilterBundle\SearchFilter\Factory $searchFilterFactory
     * @param \Engine\ModelBundle\Entity\RepositoryInterface $pagesRepository
     */
    public function __construct(SearchFilterFactory $searchFilterFactory, RepositoryInterface $pagesRepository)
    {
        $this->pagesRepository = $pagesRepository;
        $this->searchFilterFactory = $searchFilterFactory;
    }

    /**
     * @param string $url
     * @return \Engine\PageBundle\Entity\ReadPageInterface
     */
    public function getPageByUrl($url)
    {
        return $this->restoreByFilter($this->searchFilterFactory->createFilterChain([
            'i18n.withTranslations',
            'page.notSystem',
            'url' => [$url],
        ]));
    }

    /**
     * {@inheritdoc}
     */
    protected function getEntitiesRepository()
    {
        return $this->pagesRepository;
    }
}
