<?php
/**
 * {@license}
 */

namespace Engine\PageBundle\Service;

use Engine\ModelBundle\Service\FinderInterface;

/**
 * FACADE PATTERN
 * @link http://en.wikipedia.org/wiki/Facade_pattern
 *
 * Service (DDD by Eric Evans)
 * @link http://domaindrivendesign.org/node/125
 *
 * Service Layer
 * @link http://martinfowler.com/eaaCatalog/serviceLayer.html
 *
 * @author  naydav <valeriy.nayda@gmail.com>
 */
interface PageFinderInterface extends FinderInterface
{
    /**
     * @param string $url
     * @return \Engine\PageBundle\Entity\ReadPageInterface
     */
    public function getPageByUrl($url);
}
