<?php
/**
 * {@license}
 */

namespace Engine\PageBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @author  naydav <valeriy.nayda@gmail.com>
 */
class EnginePageBundle extends Bundle
{
}
