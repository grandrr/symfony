<?php
/**
 * {@license}
 */

namespace Engine\PageBundle\Entity;

use Engine\ModelBundle\Entity\EntityInterface;
use Engine\NestedEntityBundle\Entity\NestedInterface;

/**
 * @author  naydav <valeriy.nayda@gmail.com>
 */
interface ReadPageInterface extends EntityInterface, NestedInterface
{
    /**
     * @return string
     */
    public function getUrl();

    /**
     * @return bool
     */
    public function isInMenu();

    /**
     * @return bool
     */
    public function isInSiteMap();

    /**
     * @return bool
     */
    public function isSystem();

    /**
     * @return \Engine\PageBundle\Entity\ReadPageInterface[]
     */
    public function getChildren();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getContent();

    /**
     * @return \Engine\MetaTagsValueObjectBundle\Entity\ValueObject\Meta
     */
    public function getMeta();

    /**
     * Check is external page url
     *
     * @return bool
     */
    public function isHasExternalUrl();
}
