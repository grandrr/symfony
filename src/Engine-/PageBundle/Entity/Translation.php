<?php
/**
 * {@license}
 */

namespace Engine\PageBundle\Entity;

use Engine\I18nBundle\Entity\Action\TranslationTrait;
use Engine\I18nBundle\Entity\TranslationInterface;
use Engine\ModelBundle\Entity\Action as EntityAction;
use Engine\ModelBundle\Entity\EntityInterface;
use Engine\MetaTagsValueObjectBundle\Entity\Action as MetaTagsAction;
use Engine\MetaTagsValueObjectBundle\Entity\ValueObject\Meta;

/**
 * @author  naydav <valeriy.nayda@gmail.com>
 */
class Translation implements EntityInterface, TranslationInterface
{
    /**
     * Add methods
     */
    use EntityAction\IdTrait, EntityAction\TitleTrait, MetaTagsAction\MetaTrait, TranslationTrait;

    /**
     * @var string
     */
    protected $content;

    /**
     * @param string $locale
     * @param string $title
     * @param string $content
     * @param \Engine\MetaTagsValueObjectBundle\Entity\ValueObject\Meta $meta
     */
    public function __construct($locale, $title, $content, Meta $meta)
    {
        $this->setLocale($locale);
        $this->setTitle($title);
        $this->setContent($content);
        $this->setMeta($meta);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}
