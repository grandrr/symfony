<?php
/**
 * {@license}
 */

namespace Engine\PageBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Engine\I18nBundle\Entity\Action as I18nAction;
use Engine\I18nBundle\Entity\TranslatableInterface;
use Engine\MetaTagsValueObjectBundle\Entity\Action as MetaTagsAction;
use Engine\ModelBundle\Entity\Action as EntityAction;
use Engine\NestedEntityBundle\Entity\Action\NestedTrait;

/**
 * @method \Engine\PageBundle\Entity\Translation getCurrentTranslation()
 * @author  naydav <valeriy.nayda@gmail.com>
 */
class Page implements ReadPageInterface, TranslatableInterface
{
    /**
     * Add methods
     */
    use EntityAction\IdTrait, EntityAction\PositionTrait, I18nAction\TranslatableTrait, I18nAction\ProxyTitleTrait,
        MetaTagsAction\I18n\ProxyMetaTrait, NestedTrait;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var bool
     */
    protected $inMenu;

    /**
     * @var bool
     */
    protected $inSiteMap;

    /**
     * @var bool
     */
    protected $isSystem;

    /**
     * @param string $url
     * @param bool $inMenu
     * @param bool $inSiteMap
     * @param bool $isSystem
     */
    public function __construct($url, $inMenu = false, $inSiteMap = false, $isSystem = false)
    {
        $this->setUrl($url);
        $this->setInMenu($inMenu);
        $this->setInSiteMap($inSiteMap);
        $this->setIsSystem($isSystem);
        $this->position = 0;
        // set nested values
        $this->initNestedProperties();
        // collections
        $this->translations = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return bool
     */
    public function isInMenu()
    {
        return $this->inMenu;
    }

    /**
     * @param bool $inMenu
     */
    public function setInMenu($inMenu)
    {
        $this->inMenu = (bool)$inMenu;
    }

    /**
     * @return bool
     */
    public function isInSiteMap()
    {
        return $this->inSiteMap;
    }

    /**
     * @param bool $inSiteMap
     */
    public function setInSiteMap($inSiteMap)
    {
        $this->inSiteMap = (bool)$inSiteMap;
    }

    /**
     * @return bool
     */
    public function isSystem()
    {
        return $this->isSystem;
    }

    /**
     * @param bool $isSystem
     */
    public function setIsSystem($isSystem)
    {
        $this->isSystem = (bool)$isSystem;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->getCurrentTranslation()->getContent();
    }

    /**
     * Check is external page url
     *
     * @return bool
     */
    public function isHasExternalUrl()
    {
        return \strpos($this->getUrl(), 'http://') === 0;
    }
}
