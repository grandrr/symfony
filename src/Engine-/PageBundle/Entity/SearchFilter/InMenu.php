<?php
/**
 * {@license}
 */

namespace Engine\PageBundle\Entity\SearchFilter;

use Engine\SearchFilterBundle\SearchFilter\AbstractFilter;

/**
 * {@inheritdoc}
 */
class InMenu extends AbstractFilter
{
}
