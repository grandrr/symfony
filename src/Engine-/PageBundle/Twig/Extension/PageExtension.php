<?php
/**
 * {@license}
 */

namespace Engine\PageBundle\Twig\Extension;

use Engine\PageBundle\Service\PageFinderInterface;

/**
 * @link http://symfony.com/doc/current/cookbook/templating/twig_extension.html
 * @link http://twig.sensiolabs.org/doc/advanced.html#creating-an-extension
 *
 * @author  naydav <valeriy.nayda@gmail.com>
 */
class PageExtension extends \Twig_Extension
{
    /**
     * @var \Engine\PageBundle\Service\PageFinderInterface
     */
    protected $pageFinder;

    /**
     * @param \Engine\PageBundle\Service\PageFinderInterface $pageFinder
     */
    public function __construct(PageFinderInterface $pageFinder)
    {
        $this->pageFinder = $pageFinder;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'page_url_by_id',
                function ($id) {
                    return $this->pageFinder->restore($id)->getUrl();
                },
                ['is_safe' => ['html']]
            ),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'engine_page';
    }
}
