<?php
/**
 * {@license}
 */

namespace Engine\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @author naydav <valeriy.nayda@gmail.com>
 */
class IndexController extends Controller
{
    /**
     * @param string $url
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showByUrlAction($url)
    {
        /** @var \Engine\PageBundle\Service\PageFinderInterface $pageFinder */
        $pageFinder = $this->get('engine.page.pageFinder');

        $page = $pageFinder->getPageByUrl($url);

        // Meta
//        $metaTitle = $page->getMeta()->getTitle();
//        $this->_helper->metaTags([
//            'title' => !empty($metaTitle) ? $metaTitle : $page->getTitle(),
//            'description' => $page->getMeta()->getDescription(),
//            'keywords' => $page->getMeta()->getKeywords(),
//        ]);

        return $this->render('EnginePageBundle:Index:show-by-url.html.twig', ['page' => $page]);
    }
}
