<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/6/14
 * Time: 1:49 PM
 */

namespace Acme\BlogBundle\Forms\Post;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Acme\BlogBundle\Services\CategoryFinder;

class Create extends AbstractType
{

    /**
     * @var Acme\BlogBundle\Services\CategoryFinder
     */
    protected $categoryFinder;

    /**
     * @param CategoryFinder $categoryFinder
     */
    public function __construct(CategoryFinder $categoryFinder)
    {
        $this->categoryFinder = $categoryFinder;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            //disable html5 validation
            'attr' => array('novalidate' => 'novalidate'),
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention' => 'post_item',
        ));
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'label' => 'Title',
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3)),
                ),
            ))
            ->add('description', 'textarea', array(
                'label' => 'Description',
                'constraints' => array(
                    new NotBlank(),
                ),
            ))
            ->add('text', 'textarea', array(
                'label' => 'Text',
                'constraints' => array(
                    new NotBlank(),
                ),
            ))
            ->add('metaTitle', 'text', array(
                'label' => 'Meta Title',
            ))
            ->add('metaDescription', 'textarea', array(
                'label' => 'Meta Description',
            ))
            ->add('metaKeywords', 'textarea', array(
                'label' => 'Meta Keywords',
            ))
            ->add('categoryId', 'choice', array(
                'label' => 'Category',
                'constraints' => array(
                    new NotBlank(),
                ),
                'choices' => $this->categoryFinder->getCategorysForSelect(),
            ))
            ->add('save', 'submit');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'post';
    }
}