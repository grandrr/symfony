<?php

namespace Acme\BlogBundle\Entity\Post;

use Acme\EngineBundle\Entity\EntityFactoryInterface;
use Doctrine\ORM\EntityRepository;

class PostFactory implements EntityFactoryInterface
{
    /**
     * @var Repository|\Doctrine\ORM\EntityRepository
     */
    protected $categoryRepository;

    /**
     * @param EntityRepository $categoryRepository
     */
    public function __construct(EntityRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function create(array $data)
    {
        $post = new Post(
            $data['title'],
            $data['description'],
            $data['text'],
            $data['metaTitle'],
            $data['metaDescription'],
            $data['metaKeywords']
        );

        if ($data['categoryId']) {
            $category = $this->categoryRepository->find($data['categoryId']);
            $post->setCategory($category);
        }

        return $post;
    }
}
