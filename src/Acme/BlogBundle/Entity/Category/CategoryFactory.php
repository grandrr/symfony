<?php

namespace Acme\BlogBundle\Entity\Category;

use Acme\EngineBundle\Entity\EntityFactoryInterface;

class CategoryFactory implements  EntityFactoryInterface
{
    public function create(array $data)
    {
        return new Category($data['name']);
    }
}
