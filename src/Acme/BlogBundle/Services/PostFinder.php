<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Acme\BlogBundle\Services;

use Doctrine\ORM\EntityRepository;

/**
 * Class PostFinder
 * @package Acme\BlogBundle\Services
 */
class PostFinder
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $postRepository;

    /**
     * @param EntityRepository $postRepository
     */
    public function __construct(
        EntityRepository $postRepository
    )
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPostById($id)
    {
        return $this->postRepository->find($id);
    }
} 