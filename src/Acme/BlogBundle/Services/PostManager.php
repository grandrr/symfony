<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Acme\BlogBundle\Services;

use Acme\EngineBundle\Services\AbstractEntityManager;
use Doctrine\ORM\EntityRepository;

/**
 * Class PostService
 * @package Acme\BlogBundle\Services
 */
class PostManager extends AbstractEntityManager
{

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $categoryRepository;

    /**
     * @param EntityRepository $entityRepository
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param \Acme\EngineBundle\Entity\EntityFactoryInterface $entityFactory
     * @param EntityRepository $categoryRepository
     */
    public function __construct(
        $entityRepository,
        $entityManager,
        $entityFactory,
        EntityRepository $categoryRepository
    )
    {
        parent::__construct($entityRepository, $entityManager, $entityFactory);

        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param array $data
     * @return bool|mixed|null|object
     */
    public function updateFromValidData(array $data)
    {
        if ($entity = $this->updateData($data)) {
            $category = $this->categoryRepository->find($data['categoryId']);
            if ($category)
                $entity->setCategory($category);

            return $this->saveEntity($entity);
        }
        return false;
    }
} 