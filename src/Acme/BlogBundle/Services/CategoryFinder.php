<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Acme\BlogBundle\Services;

use Acme\BlogBundle\Entity\Category\CategoryRepository;

/**
 * Class CategoryFinder
 * @package Acme\BlogBundle\Services
 */
class CategoryFinder
{
    /**
     * @var \Acme\BlogBundle\Entity\Category\CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(
        CategoryRepository $categoryRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return array
     */
    public function getCategorysForSelect()
    {
        $categorys = array();
        foreach ($this->categoryRepository->findAll() as $category)
            $categorys[$category->getId()] = $category->getName();

        return $categorys;
    }

    public function getAll()
    {
        return $this->categoryRepository->findAll();
    }

    public function getCategoryById($id)
    {
        return $this->categoryRepository->find($id);
    }
} 