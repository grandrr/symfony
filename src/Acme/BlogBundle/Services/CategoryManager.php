<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/5/14
 * Time: 1:18 PM
 */

namespace Acme\BlogBundle\Services;

use Acme\EngineBundle\Services\AbstractEntityManager;
use Doctrine\ORM\EntityManager;
use Acme\BlogBundle\Entity\Category\CategoryFactory;

/**
 * Class CategoryService
 * @package Acme\BlogBundle\Services
 */
class CategoryManager extends AbstractEntityManager
{
//    /**
//     * @var \Doctrine\ORM\EntityManager
//     */
//    protected $entityManager;
//
//    /**
//     * @var \Acme\BlogBundle\Entity\Category\CategoryFactory
//     */
//    protected $categoryFactory;
//
//    /**
//     * @param EntityManager $entityManager
//     * @param CategoryFactory $categoryFactory
//     */
//    public function __construct(
//        EntityManager $entityManager,
//        CategoryFactory $categoryFactory
//    )
//    {
//        $this->entityManager = $entityManager;
//        $this->categoryFactory = $categoryFactory;
//    }

//    /**
//     * @param $request
//     */
//    public function createFromRequest($request)
//    {
//        $this->create($request->request->get('category')['name']);
//    }

//    /**
//     * @param $request
//     * @return mixed
//     */
//    protected function create(array $data)
//    {
//        $entity = $this->entityFactory->create($data);
//        $this->entityManager->persist($entity);
//        $this->entityManager->flush();
//        return $entity;
//    }
} 