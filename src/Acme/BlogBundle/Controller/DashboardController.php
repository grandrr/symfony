<?php

namespace Acme\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DashboardController extends Controller
{
    public function createPostAction(Request $request)
    {
        $formPostCreateType = $this->get('acme.blog.forms.post.create');
        $form = $this->createForm($formPostCreateType);

        $success = false;
        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $postManager = $this->get('acme.blog.services.post_manager');
                $postManager->createFromValidData($form->getData());
                $success = true;
            }
        }

        return $this->render('AcmeBlogBundle:Dashboard:createPost.html.twig', array(
            'form' => $form->createView(),
            'success' => $success,
        ));
    }

    public function editPostAction(Request $request)
    {
        $formPostEditType = $this->get('acme.blog.forms.post.edit');
        $form = $this->createForm($formPostEditType);

        $success = false;
        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $postManager = $this->get('acme.blog.services.post_manager');
                $postManager->updateFromValidData($form->getData());
                $success = true;
            }
        } else {
            $repository = $this->get('acme.blog.entity.post.post_repository');
            $post = $repository->find($request->get('id'));
            $form->setData(array(
                'id' => $post->getId(),
                'title' => $post->getTitle(),
                'text' => $post->getText(),
                'description' => $post->getDescription(),
                'metaTitle' => $post->getMetaTitle(),
                'metaDescription' => $post->getMetaDescription(),
                'metaKeywords' => $post->getMetaKeywords(),
                'categoryId' => $post->getCategory()->getId(),
            ));
        }

        return $this->render('AcmeBlogBundle:Dashboard:editPost.html.twig', array(
            'form' => $form->createView(),
            'success' => $success,
        ));
    }

    public function createCategoryAction(Request $request)
    {
        $formCategoryCreateType = $this->get('acme.blog.forms.category.create');
        $form = $this->createForm($formCategoryCreateType);

        $success = false;
        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $categoryManager = $this->get('acme.blog.services.category_manager');
                $categoryManager->createFromValidData($form->getData());
                $success = true;
            }
        }

        return $this->render('AcmeBlogBundle:Dashboard:createCategory.html.twig', array(
            'form' => $form->createView(),
            'success' => $success,
        ));
    }

    public function editCategoryAction(Request $request)
    {
        $formCategoryEditType = $this->get('acme.blog.forms.category.edit');
        $form = $this->createForm($formCategoryEditType);

        $success = false;
        if ($request->isMethod('post')) {
            $form->submit($request);

            if ($form->isValid()) {
                $categoryManager = $this->get('acme.blog.services.category_manager');
                $categoryManager->updateFromValidData($form->getData());
                $success = true;
            }
        } else {
            $repository = $this->get('acme.blog.entity.category.category_repository');
            $category = $repository->find($request->get('id'));
            $form->setData(array(
                'id' => $category->getId(),
                'name' => $category->getName(),
            ));
        }

        return $this->render('AcmeBlogBundle:Dashboard:editCategory.html.twig', array(
            'form' => $form->createView(),
            'success' => $success,
        ));
    }
}
