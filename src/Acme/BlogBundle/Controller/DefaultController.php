<?php

namespace Acme\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\Tools\Pagination\Paginator;

class DefaultController extends Controller
{
    public function indexAction($page)
    {
        $postRepository = $this->get('acme.blog.entity.post.post_repository');
        $maxArticles = $this->container->getParameter('max_articles_per_page');
        $query = $postRepository->createQueryBuilder('p')
            ->orderBy('p.date', 'ASC')
            ->setFirstResult(($page-1)*$maxArticles)
            ->setMaxResults($maxArticles)
            ->getQuery();

        $posts = new Paginator($query, $fetchJoinCollection = true);

        $articles_count = 5;
        $pagination = array(
            'page' => $page,
            'route' => 'acme_blog_homepage',
            'pages_count' => ceil($articles_count / $maxArticles),
            'route_params' => array()
        );

        return $this->render('AcmeBlogBundle:Default:index.html.twig',
            array(
                'posts' => $posts,
                'pagination' => $pagination,
            ));
    }

    public function postByIdAction($id)
    {
        $postFinder = $this->get('acme.blog.services.post_finder');
        $post = $postFinder->getPostById($id);

//        $securityContext = $this->container->get('security.context');
//        if( $securityContext->isGranted('ROLE_ADMIN') ){
//            // authenticated REMEMBERED, FULLY will imply REMEMBERED (NON anonymous)
//        }

        return $this->render('AcmeBlogBundle:Default:postById.html.twig', array(
            'post' => $post,
        ));
    }

    public function categorysAction()
    {
        $categoryFinder = $this->get('acme.blog.services.category_finder');
        $categorys = $categoryFinder->getAll();

        return $this->render('AcmeBlogBundle:Default:categorys.html.twig', array('categorys' => $categorys));
    }

    public function postsByCategoryAction($id)
    {
        $categoryFinder = $this->get('acme.blog.services.category_finder');
        $category = $categoryFinder->getCategoryById($id);
        $posts = $category->getPosts();
        return $this->render('AcmeBlogBundle:Default:postsByCategory.html.twig', array(
            'posts' => $posts,
            'category' => $category,
        ));
    }
}
