<?php

namespace Acme\EngineBundle\Entity;

interface EntityFactoryInterface
{
    public function create(array $data);

}
