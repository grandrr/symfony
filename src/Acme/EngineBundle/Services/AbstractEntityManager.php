<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/13/14
 * Time: 12:53 PM
 */

namespace Acme\EngineBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Acme\EngineBundle\Entity\EntityFactoryInterface;

abstract class AbstractEntityManager
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $entityRepository;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Acme\EngineBundle\Entity\Factory
     */
    protected $entityFactory;

    /**
     * @param EntityRepository $entityRepository
     * @param EntityManager $entityManager
     * @param EntityFactoryInterface $entityFactory
     */
    public function __construct(
        EntityRepository $entityRepository,
        EntityManager $entityManager,
        EntityFactoryInterface $entityFactory
    )
    {
        $this->entityRepository = $entityRepository;
        $this->entityManager = $entityManager;
        $this->entityFactory = $entityFactory;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createFromValidData(array $data)
    {
        $entity = $this->entityFactory->create($data);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return $entity;
    }

    /**
     * @param array $data
     * @return null|object
     */
    public function updateFromValidData(array $data)
    {
        if ($entity = $this->updateData($data)) {
            return $this->saveEntity($entity);
        }
        return false;
    }

    /**
     * @param array $data
     * @return bool|null|object
     */
    protected function updateData(array $data)
    {
        if ($entity = $this->entityRepository->find($data['id'])) {
            unset($data['id']);
            foreach ($data as $field => $value) {
                $funtion = "set$field";
                if (\method_exists($entity, $funtion))
                    $entity->$funtion($value);
            }
            return $entity;
        }
        return false;
    }

    /**
     * @param $entity
     * @return mixed
     */
    protected function saveEntity($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return $entity;
    }

} 